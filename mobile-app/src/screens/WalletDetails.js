
import React from 'react';
import { WTransactionHistory } from '../components';
import {
  StyleSheet,
  View,
  Text,
  TouchableWithoutFeedback,
  Dimensions,
  Alert
} from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { colors } from '../common/theme';
var { height } = Dimensions.get('window');
import { language } from 'config';
import { useSelector } from 'react-redux';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { LinearGradient } from 'expo-linear-gradient';

export default function WalletDetails(props) {

  const auth = useSelector(state => state.auth);
  const settings = useSelector(state => state.settingsdata.settings);
  const providers = useSelector(state => state.paymentmethods.providers);

  const doReacharge = () => {
    if (providers) {
      props.navigation.push('addMoney', { userdata: auth.info.profile, providers: providers });
    } else {
      Alert.alert(language.alert,language.provider_not_found)
    }
  }

  const doWithdraw = () => {
    if (auth.info.profile.walletBalance>0) {
      props.navigation.push('withdrawMoney', { userdata: auth.info.profile });
    } else {
      Alert.alert(language.alert,language.wallet_zero)
    }
  }

  const walletBar = height / 4;
  return (
    <View style={styles.mainView}>
      <Header
        backgroundColor={colors.WHITE}
        leftComponent={{ icon: 'align-left', type: 'feather', color: '#af31f8', size: 25, component: TouchableWithoutFeedback, onPress: () => { props.navigation.toggleDrawer(); } }}
        rightComponent={ auth.info && auth.info.profile && auth.info.profile.usertype =='driver'?<TouchableOpacity onPress={doWithdraw}><Text style={{color:colors.WHITE}}>{language.withdraw}</Text></TouchableOpacity>:null}
        centerComponent={<Text style={styles.headerTitleStyle}>{language.my_wallet_tile}</Text>}
        containerStyle={styles.headerStyle}
        innerContainerStyles={{ marginLeft: 10, marginRight: 10 }}
      />


      <View style={{flex:1, maxHeight:'18%', flexDirection:'column', justifyContent:'center', alignItems:'center', marginHorizontal:10, marginVertical:15, borderRadius:16, backgroundColor:'#f9f9f9', 
                        shadowOpacity: 0.1,
                        shadowRadius: 4,
                        shadowOffset: {
                            height: 0,
                            width: 0
                        },
                        }}>
        
        
        {/* Wallet Balance */}
        <View style={{justifyContent:'center', marginBottom:8}}>
         <Text style={{ color:'#af31f8', fontSize: 16 }}>{language.wallet_ballance}</Text>
        </View>
        {/* Wallet Balance */}

        {/* Balance Number */}
        <View style={{justifyContent:'center', marginBottom:8}}>
         <Text style={{fontSize: 22, fontWeight: '600', color: colors.GREY.default }}>{settings.symbol}{auth.info && auth.info.profile ? parseFloat(auth.info.profile.walletBalance).toFixed(2) : ''}</Text>
        </View>
        {/* Balance Number */}

        {/* Add Funds */}
       {auth.info.profile.usertype=='driver' && <View style={{justifyContent:'center', marginBottom:8}}>
             <TouchableWithoutFeedback onPress={doReacharge}>
              <LinearGradient
                  colors={['#4eaefd', '#8a61fa']}
                  style={{flexDirection:'row', paddingHorizontal:8, paddingVertical:6, borderRadius:13,}}
                >
                  <Icon
                      name='add-circle'
                      type='MaterialIcons'
                      color={colors.WHITE}
                      size={14}
                      iconStyle={{marginRight:4}}
                    />
                    <Text style={{ textAlign: 'center', fontSize: 13, color: colors.WHITE }}>{language.add_money}</Text>
              </LinearGradient>
                
              </TouchableWithoutFeedback>
        </View> }
        {/* Add Funds */}


      </View>
      


      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={{}}>
          
          <View style={{ marginVertical: 10 }}>
            <Text style={{ paddingHorizontal: 10, fontSize: 16, fontWeight: '500', marginTop: 8 }}>{language.transaction_history_title}</Text>
          </View>
        </View>

        <View style={{}}>
          <View style={{ height: (height - walletBar) - 110 }}>
            <WTransactionHistory walletHistory={auth.info.profile.walletHistory}/>
          </View>
        </View>
      </View>

    </View>
  );

}

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: colors.WHITE,
    borderBottomWidth: 0
  },
  headerTitleStyle: {
    color: colors.GREY.default,
    fontFamily: 'Roboto-Bold',
    fontSize: 16
  },

  textContainer: {
    textAlign: "center"
  },
  mainView: {
    flex: 1,
    backgroundColor: colors.WHITE,
  },

});
