
import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableWithoutFeedback,
  ScrollView,
  TextInput,
  FlatList,
  ActivityIndicator
} from 'react-native';
import { Header } from 'react-native-elements';
import { colors } from '../common/theme';

import { language } from 'config';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useSelector } from 'react-redux';
import { WebView } from 'react-native-webview';

export default function AddMoneyScreen(props) {

  const settings = useSelector(state => state.settingsdata.settings);

  const [invoiceUrl,setInvoiceUrl] = useState(null);
  const [invoiceKey,setInvoiceKey] = useState(null);
  const [invoiceId,setInvoiceId] = useState(null);
  const [showHideWbview,setShowHideWebview] = useState(false);

  const [payByFaLoader,setPayByFaLoader] = useState(false);
  const [state, setState] = useState({
    userdata: props.navigation.getParam('userdata'),
    providers: props.navigation.getParam('providers'),
    amount: '2',
    first_name:'',last_name:'',email:'',phone:'',address:'',
    qickMoney: [{ amount: '2', selected: false },{ amount: '4', selected: false },{ amount: '6', selected: false },{ amount: '8', selected: false }, { amount: '10', selected: false }, { amount: '20', selected: false }, { amount: '50', selected: false }, { amount: '100', selected: false }],
  });

  const quckAdd = (index) => {
    let quickM = state.qickMoney;
    for (let i = 0; i < quickM.length; i++) {
      quickM[i].selected = false;
      if (i == index) {
        quickM[i].selected = true;
      }
    }
    setState({
      ...state,
      amount: quickM[index].amount,
      qickMoney: quickM
    })
  }

  const payNow = () => {
    var d = new Date();
    var time = d.getTime();
    let payData = {
      email: state.userdata.email,
      amount: state.amount,
      order_id: time.toString(),
      name: language.add_money,
      description: language.wallet_ballance,
      currency: settings.code,
      quantity: 1,
      paymentType: 'walletCredit'
    }
    if (payData) {
      props.navigation.navigate("paymentMethod", {
        payData: payData,
        userdata: state.userdata,
        settings: state.settings,
        providers: state.providers
      });
    }
  }

  const payNowByFA = () => {

    var result = state.amount % 2;
    if(state.amount == '' || state.amount <=0)
    {
      alert("Please enter amount");
    }
    else if(state.first_name=="")
    {
      alert("Please enter first name");
    }
    else if(state.last_name=="")
    {
      alert("Please enter last name");
    }
    else if(state.email=="")
    {
      alert("Please enter email");
    }
    else if(state.phone=="")
    {
      alert("Please enter phone number");
    }
    else if(state.address=="")
    {
      alert("Please enter address");
    }
    else if(result != 0)
    {
      alert("You can not add odd amount to wallet,Please enter even amount!!")
      
        
     
    }
    else
    {
      let qty = parseInt(state.amount/2);
      
      setPayByFaLoader(true);
      var formdata = new FormData();
      //formdata.append("vendorKey", "5fbb30cedd554187680aa9b54b9b8990390a4d9bafed0dd388");
      formdata.append("vendorKey", "f6cc3eba9272a4120f83f50c6eca8f9f25d60f41255bf229eb");
      
      formdata.append("cartItems[0][name]", "AddToWallet");
      formdata.append("cartItems[0][price]", "2");
      formdata.append("cartItems[0][quantity]",qty);
      formdata.append("cartTotal", state.amount);
      formdata.append("shipping", "0");
      formdata.append("due_date","2025/01/31")
      formdata.append("customer[first_name]", state.first_name);
      formdata.append("customer[last_name]", state.last_name);
      formdata.append("customer[email]", state.email);
      formdata.append("customer[phone]", state.phone);
      formdata.append("customer[address]",state.address);
      //formdata.append("redirectUrl", "https://app.fawaterk.com/invoices/item/3549/addtowallet");
      formdata.append("redirectUrl", "https://app.fawaterk.com/invoices/item/3758/addtowallet");
     
      formdata.append("currency", "EGP");
      formdata.append("sendSMS", "true");
      formdata.append("sendEmail", "true");
      formdata.append("pay_load", "FTest");
      
      var requestOptions = {
        method: 'POST',
        body: formdata,
        redirect: 'follow'
      };
      
      fetch("https://app.fawaterk.com/api/invoice", requestOptions)
        .then(response => response.json())
        .then(result => { 
          console.log(result);
          console.log(result.url)
          console.log(result.invoiceKey)
          setInvoiceUrl(result.url);
          setInvoiceKey(result.invoiceKey);
          setInvoiceId(result.invoiceId);
          setShowHideWebview(true);
          
          
        })
        .catch(error => {
          console.log('error', error);
          alert("Something went wrong,try after sometime")
        })
        .finally(()=>setPayByFaLoader(false)); 
      
    }


    
  }

 

  const newData = ({ item, index }) => {
    return (
      <TouchableOpacity style={[styles.boxView, { backgroundColor: item.selected ? colors.GREY.default : colors.GREY.primary }]} onPress={() => { quckAdd(index); }}><Text style={styles.quckMoneyText, { color: item.selected ? colors.WHITE : colors.BLACK }} >{settings.symbol}{item.amount}</Text></TouchableOpacity>
    )
  }


  //go back
  const goBack = () => {
    props.navigation.goBack();
  }


  const IndicatorLoadingView = () => {
    return (
      <ActivityIndicator
        color="#3235fd"
        size="large"
        style={styles.IndicatorStyle}
      />
    );
  }
  const onNavigationStateChange = (webViewState) =>{
   

   
    /* var loadedUrl = webViewState.url;
    var myurl = loadedUrl.toString().replace("?invoice_id="+invoiceId,"");
    console.log('loaded url:'+myurl)
    if(myurl == 'https://drivalegypt-app.web.app/successaddtowallet')
    {
console.log('inside if')
      props.navigation.push('wallet');
    } */
  }


  return (
    <View style={styles.mainView}>
      <Header
        backgroundColor={colors.GREY.default}
        leftComponent={{ icon: 'ios-arrow-back', type: 'ionicon', color: colors.WHITE, size: 30, component: TouchableWithoutFeedback, onPress: () => { goBack() } }}
        centerComponent={<Text style={styles.headerTitleStyle}>{language.add_money_tile}</Text>}
        containerStyle={styles.headerStyle}
        innerContainerStyles={{ marginLeft: 10, marginRight: 10 }}
      />

     {!showHideWbview && <View style={styles.bodyContainer}>
        <Text style={styles.walletbalText}>{language.Balance} - <Text style={styles.ballance}>{settings.symbol}{state.userdata ? parseFloat(state.userdata.walletBalance).toFixed(2) : ''}</Text></Text>

        <TextInput
          style={styles.inputTextStyle}
          placeholder={language.addMoneyTextInputPlaceholder + " (" + settings.symbol + ")"}
          keyboardType={'number-pad'}
          onChangeText={(text) => setState({ ...state,amount: text.replace(/[^0-9]/g, '') })}
          value={state.amount}
        />
        <View style={styles.quickMoneyContainer}>
          <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
            <FlatList
              keyExtractor={(item, index) => index.toString()}
              data={state.qickMoney}
              renderItem={newData}
              horizontal={true}
            />
          </ScrollView>
        </View>
         <TextInput
          style={styles.inputTextStyle2}
          placeholder="First Name"
          
          onChangeText={(text) => setState({ ...state,first_name: text })}
          value={state.first_name}
        />
        <TextInput
          style={styles.inputTextStyle2}
          placeholder="Last Name"
          
          onChangeText={(text) => setState({ ...state,last_name: text })}
          value={state.last_name}
        />
        <TextInput
          style={styles.inputTextStyle2}
          placeholder="E-Mail"
          
          onChangeText={(text) => setState({ ...state,email: text })}
          value={state.email}
        />
         <TextInput
          style={styles.inputTextStyle2}
          placeholder="Phone"
          keyboardType={'number-pad'}
          onChangeText={(text) => setState({ ...state,phone: text })}
          value={state.phone}
        />
        <TextInput
          style={styles.inputTextStyle2}
          placeholder="Address"
          
          onChangeText={(text) => setState({ ...state,address: text })}
          value={state.address}
        />
        
        {/* <TouchableOpacity
          style={styles.buttonWrapper2}
          onPress={payNow}>
          <Text style={styles.buttonTitle}>{language.add_money_tile}</Text>
        </TouchableOpacity> */}
        {!payByFaLoader && <TouchableOpacity
          style={styles.buttonWrapper2}
          onPress={payNowByFA}>
          <Text style={styles.buttonTitle}>Pay By FAWATERAK</Text>
        </TouchableOpacity>}

        {payByFaLoader && <TouchableOpacity
          style={styles.buttonWrapper2}
          >
          <ActivityIndicator size="small" color="#ffffff" />
        </TouchableOpacity>}


      </View>}
      {showHideWbview && <View style={styles.bodyContainer}>
      <WebView
   key={invoiceUrl}   
   style={styles.webview}
   onLoad={() => console.log('webview loading finished')}
   source={{uri:invoiceUrl}}
   onNavigationStateChange={onNavigationStateChange}
   javaScriptEnabled={true}
   domStorageEnabled={true}
   startInLoadingState={true}
   
   scalesPageToFit={false} />
       
       </View>}
    </View>
  );

}

const styles = StyleSheet.create({

  headerStyle: {
    backgroundColor: colors.GREY.default,
    borderBottomWidth: 0
  },
  headerTitleStyle: {
    color: colors.WHITE,
    fontFamily: 'Roboto-Bold',
    fontSize: 20
  },

  mainView: {
    flex: 1,
    backgroundColor: colors.WHITE,
  },
  bodyContainer: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 10,
    paddingHorizontal: 12
  },
  walletbalText: {
    fontSize: 17
  },
  ballance: {
    fontWeight: 'bold'
  },
  inputTextStyle: {
    marginTop: 10,
    height: 50,
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    fontSize: 30
  },
  inputTextStyle2: {
    marginTop: 10,
    height: 50,
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    fontSize: 20
  },
  buttonWrapper2: {
    marginBottom: 10,
    marginTop: 18,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.GREY.default,
    borderRadius: 8,
  },
  buttonTitle: {
    color: colors.WHITE,
    fontSize: 18,
  },
  quickMoneyContainer: {
    marginTop: 18,
    flexDirection: 'row',
    paddingVertical: 4,
    paddingLeft: 4,
  },
  boxView: {
    height: 40,
    width: 60,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 8
  },
  quckMoneyText: {
    fontSize: 16,
  },
  IndicatorStyle: {
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  },
  webview: {
    flex: 1,
    backgroundColor: 'yellow',
    width: "100%",
    
  }

});
