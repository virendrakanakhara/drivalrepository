import React, { useEffect, useState, useRef, useContext } from 'react';
import {
    StyleSheet,
    View,
    Image,
    Dimensions,
    Text,
    Platform,
    Alert,
    Modal,
    ScrollView,TouchableHighlight ,AsyncStorage 
} from 'react-native';
import { TouchableOpacity, BaseButton, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { MapComponent } from '../components';
import { Icon, Header, Tooltip } from 'react-native-elements';
import { colors } from '../common/theme';
import * as Location from 'expo-location';
var { height, width } = Dimensions.get('window');
import { language } from 'config';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { useSelector, useDispatch } from 'react-redux';
import { NavigationEvents } from 'react-navigation';
import { store, FirebaseContext } from 'common/src';
import { LinearGradient } from 'expo-linear-gradient';

export default function MapScreen(props) {
    const { api } = useContext(FirebaseContext);
    const {
        fetchAddressfromCoords,
        fetchDrivers,
        updateTripPickup,
        updateTripDrop,
        updatSelPointType,
        getDriveTime,
        GetDistance,
        MinutesPassed,
        updateTripCar,
        getEstimate,
        clearEstimate
    } = api;
    const dispatch = useDispatch();

    const auth = useSelector(state => state.auth);
    const settings = useSelector(state => state.settingsdata.settings);
    const cars = useSelector(state => state.cartypes.cars);
    const tripdata = useSelector(state => state.tripdata);
    const drivers = useSelector(state => state.usersdata.users);
    const estimatedata = useSelector(state => state.estimatedata);
    const activeBookings = useSelector(state => state.bookinglistdata.active);
    const gps = useSelector(state => state.gpsdata);

    const latitudeDelta = 0.0922;
    const longitudeDelta = 0.0421;

    const [allCarTypes,setAllCarTypes] = useState([]);
    const [freeCars, setFreeCars] = useState([]);
    const [pickerConfig,setPickerConfig] = useState({
        selectedDateTime: new Date(),
        dateModalOpen: false,
        dateMode: 'date'
    });
    const [loadingModal, setLoadingModal] = useState(false);
    const [mapMoved,setMapMoved] = useState(false);
    const [region,setRegion] = useState(null);
    const pageActive = useRef(false);

    const [modalVisible, setModalVisible] = useState(false);

    useEffect(()=>{


        const value = AsyncStorage.getItem('once');

        if (value !== null) {
            value.then((ret) => {
              if (ret === null) {
                // this is the first time
                // show the modal
                // save the value
          
                AsyncStorage.setItem('once', 'yes');
                setModalVisible(true)
          
              } else {
                // this is the second time
                // skip the modal
          
              }
            }).catch(err => alert(err.toString()));
          }
        
    },[]);

    useEffect(() => {
        if (cars) {
            resetCars();
        }
    },[cars]);

    useEffect(() => {
        if (tripdata.pickup && drivers) {
            resetCars();
            getDrivers();
        }
        if (tripdata.pickup && !drivers) {
            resetCars();
            setFreeCars([]);
        }
    }, [drivers, tripdata.pickup]);

    useEffect(()=>{
        if(estimatedata.estimate){
            resetActiveCar();
            props.navigation.navigate('FareDetails',{estimatefare : estimatedata.estimate.estimateFare});
        }
        if(estimatedata.error && estimatedata.error.flag){
            Alert.alert(estimatedata.error.msg);
            dispatch(clearEstimate());
        }
    },[estimatedata.estimate,estimatedata.error, estimatedata.error.flag]);
 
    useEffect(()=>{
        if(tripdata.selected &&  tripdata.selected == 'pickup' && tripdata.pickup && !mapMoved && tripdata.pickup.source == 'search'){
            setRegion({
                latitude: tripdata.pickup.lat,
                longitude: tripdata.pickup.lng,
                latitudeDelta: latitudeDelta,
                longitudeDelta: longitudeDelta
            });
        }
        if(tripdata.selected &&  tripdata.selected == 'drop' && tripdata.drop  && !mapMoved && tripdata.drop.source == 'search'){
            setRegion({
                latitude: tripdata.drop.lat,
                longitude: tripdata.drop.lng,
                latitudeDelta: latitudeDelta,
                longitudeDelta: longitudeDelta
            });
        }
    },[tripdata.selected,tripdata.pickup,tripdata.drop]);


    useEffect(()=>{
        setLoadingModal(true);
        setInterval(() => {
            if(pageActive.current){
                dispatch(fetchDrivers());
            }
        }, 30000);
    },[])

    useEffect(() => {  
        if(gps.location){  
            setRegion({
                latitude: gps.location.lat,
                longitude: gps.location.lng,
                latitudeDelta: latitudeDelta,
                longitudeDelta: longitudeDelta
            });
            updateMap({
                latitude: gps.location.lat,
                longitude: gps.location.lng
            },tripdata.pickup?'geolocation':'init');
        }
    }, [gps.location]);

    const resetCars = () => {
        let carWiseArr = [];
        for (let i = 0; i < cars.length; i++) {
            let temp = { ...cars[i], minTime: '', available: false, active: false };
            carWiseArr.push(temp);
        }
        setAllCarTypes(carWiseArr);
        selectCarType(carWiseArr[0],0);
    }

    const resetActiveCar = () => {
        let carWiseArr = [];
        for (let i = 0; i < allCarTypes.length; i++) {
            let temp = { ...allCarTypes[i], active: false };
            carWiseArr.push(temp);
        }
        setAllCarTypes(carWiseArr);
        //selectCarType(carWiseArr[0],0);
    }

    const locateUser = async () => {
        if(tripdata.selected == 'pickup'){
            setLoadingModal(true);
            let location = await Location.getCurrentPositionAsync({});
            if (location) {
                store.dispatch({
                    type: 'UPDATE_GPS_LOCATION',
                    payload: {
                        lat: location.coords.latitude,
                        lng: location.coords.longitude
                    }
                });
            }else{
                setLoadingModal(false);
            }
        }
    }

    const updateMap = async (pos,source) => {
        let latlng = pos.latitude + ',' + pos.longitude;
        fetchAddressfromCoords(latlng).then((res) => {
            if (res) {
                if (tripdata.selected == 'pickup') {
                    dispatch(updateTripPickup({
                        lat: pos.latitude,
                        lng: pos.longitude,
                        add: res,
                        source: source
                    }));
                    if(source == 'init'){
                        dispatch(updateTripDrop({
                            lat: pos.latitude,
                            lng: pos.longitude,
                            add: null,
                            source: source
                        }));
                    }
                } else {
                    dispatch(updateTripDrop({
                        lat: pos.latitude,
                        lng: pos.longitude,
                        add: res,
                        source: source
                    }));
                }
            }
            setLoadingModal(false);
        });
    }  

    const onRegionChangeComplete = (newregion) => {
        setRegion(newregion);
        if (mapMoved) {
            setMapMoved(false);
            updateMap({
                latitude: newregion.latitude,
                longitude: newregion.longitude
            },'region-change');
        }
    }

    const onPanDrag = (coordinate, position) => {
        if (!mapMoved) {
            setMapMoved(true);
        }
    }

    const selectCarType = (value, key) => {
        let carTypes = allCarTypes;
        for (let i = 0; i < carTypes.length; i++) {
            carTypes[i].active = false;
            if (carTypes[i].name == value.name) {
                carTypes[i].active = true;
            } else {
                carTypes[i].active = false;
            }
        }
        setAllCarTypes(carTypes);
        dispatch(updateTripCar(value));
    }

    const getDrivers = async () => {
        if (tripdata.pickup) {
            let availableDrivers = [];
            let arr = {};
            let startLoc = '"' + tripdata.pickup.lat + ', ' + tripdata.pickup.lng + '"';
            for (let i = 0; i < drivers.length; i++) {
                let driver = { ...drivers[i] };
                if ((driver.usertype) && (driver.usertype == 'driver') && (driver.approved == true) && (driver.queue == false) && (driver.driverActiveStatus == true)) {
                    if (driver.location) {
                        let distance = GetDistance(tripdata.pickup.lat, tripdata.pickup.lng, driver.location.lat, driver.location.lng);
                        if(settings.convert_to_mile){
                            distance = distance / 1.609344;
                        }
                        if (distance < 60) {
                            let destLoc = '"' + driver.location.lat + ', ' + driver.location.lng + '"';
                            driver.arriveDistance = distance;
                            driver.arriveTime = await getDriveTime(startLoc, destLoc);
                            let carType = driver.carType;
                            if (arr[carType] && arr[carType].drivers) {
                                arr[carType].drivers.push(driver);
                                if (arr[carType].minDistance > distance) {
                                    arr[carType].minDistance = distance;
                                    arr[carType].minTime = driver.arriveTime.timein_text;
                                }
                            } else {
                                arr[carType] = {};
                                arr[carType].drivers = [];
                                arr[carType].drivers.push(driver);
                                arr[carType].minDistance = distance;
                                arr[carType].minTime = driver.arriveTime.timein_text;
                            }
                            availableDrivers.push(driver);

                        }
                    }
                }
            }
            let carWiseArr = [];

            for (let i = 0; i < cars.length; i++) {
                let temp = { ...cars[i] };
                if (arr[cars[i].name]) {
                    temp['nearbyData'] = arr[cars[i].name].drivers;
                    temp['minTime'] = arr[cars[i].name].minTime;
                    temp['available'] = true;
                } else {
                    temp['minTime'] = '';
                    temp['available'] = false;
                }
                temp['active'] = (tripdata.carType && (tripdata.carType.name == cars[i].name)) ? true : false;
                carWiseArr.push(temp);
            }
            
            setFreeCars(availableDrivers);
            setAllCarTypes(carWiseArr);

            availableDrivers.length == 0 ? showNoDriverAlert() : null;
        }
    }

    const showNoDriverAlert = () => {
        if (tripdata.pickup && (tripdata.pickup.source == 'search' || tripdata.pickup.source == 'region-change') && tripdata.selected == 'pickup') {
            Alert.alert(
                language.no_driver_found_alert_title,
                language.no_driver_found_alert_messege,
                [
                    {
                        text: language.no_driver_found_alert_OK_button,
                        onPress: () => setLoadingModal(false),
                    }/*,
                    { 
                        text: language.no_driver_found_alert_TRY_AGAIN_button, 
                        onPress: () => { 
                            setLoadingModal(true);
                            updateMap({
                                latitude: tripdata.pickup.lat,
                                longitude: tripdata.pickup.lng
                            },'try-again'); 
                        }, 
                        style: 'cancel'
                    }*/
                ],
                { cancelable: true },
            )
        }

    }

    const tapAddress = (selection) => {
        if (selection === tripdata.selected) {
            let savedAddresses = [];
            let allAddresses = auth.info.profile.savedAddresses;
            for (let key in allAddresses) {
                savedAddresses.push(allAddresses[key]);
            }
            if (selection == 'drop') {
                props.navigation.navigate('Search', { locationType: "drop", savedAddresses: savedAddresses });
            } else {
                props.navigation.navigate('Search', { locationType: "pickup", savedAddresses: savedAddresses  });
            }
        } else {
            dispatch(updatSelPointType(selection));
            if (selection == 'drop') {
                setRegion({
                    latitude: tripdata.drop.lat,
                    longitude: tripdata.drop.lng,
                    latitudeDelta: latitudeDelta,
                    longitudeDelta: longitudeDelta
                });
            } else {
                setRegion({
                    latitude: tripdata.pickup.lat,
                    longitude: tripdata.pickup.lng,
                    latitudeDelta: latitudeDelta,
                    longitudeDelta: longitudeDelta
                });
            }
        }

    };

    //Go to confirm booking page
    const onPressBook = () => {
        if (tripdata.pickup && tripdata.drop && tripdata.drop.add) {
            if (!tripdata.carType) {
                Alert.alert(language.alert, language.car_type_blank_error)
            } else {
                let driver_available = false;
                for (let i = 0; i < allCarTypes.length; i++) {
                    let car = allCarTypes[i];
                    if (car.name == tripdata.carType.name && car.minTime) {
                        driver_available = true;
                        break;
                    }
                }
                if (driver_available) {
                    dispatch(getEstimate({
                        bookLater: false,
                        bookingDate: null,
                        pickup: {coords: {lat:tripdata.pickup.lat, lng:tripdata.pickup.lng} , description: tripdata.pickup.add},
                        drop:  {coords: {lat:tripdata.drop.lat, lng:tripdata.drop.lng}, description: tripdata.drop.add},
                        carDetails: tripdata.carType,
                    }));
                } else {
                    Alert.alert(language.alert, language.no_driver_found_alert_messege);
                }
            }
        } else {
            Alert.alert(language.alert, language.drop_location_blank_error);
        }
    }


    const onPressBookLater = () => {
        if (tripdata.pickup && tripdata.drop && tripdata.drop.add) {
            if (tripdata.carType) {
                setPickerConfig({
                    dateMode: 'date', 
                    dateModalOpen: true,
                    selectedDateTime: pickerConfig.selectedDateTime
                });
            } else {
                Alert.alert(language.alert, language.car_type_blank_error)
            }
        } else {
            Alert.alert(language.alert, language.drop_location_blank_error)
        }
    }

    const hideDatePicker = () => {
        setPickerConfig({
            dateModalOpen: false, 
            selectedDateTime: pickerConfig.selectedDateTime,
            dateMode: 'date'
        })
    };

    const handleDateConfirm = (date) => {        
        if (pickerConfig.dateMode === 'date') {
            setPickerConfig({
                dateModalOpen: false, 
                selectedDateTime: date,
                dateMode:pickerConfig.dateMode
            })
            setTimeout(() => {
                setPickerConfig({
                    dateModalOpen: true, 
                    selectedDateTime: date,
                    dateMode: 'time'
                })
            }, 1000);
        } else {
            setPickerConfig({
                dateModalOpen: false, 
                selectedDateTime: date,
                dateMode: 'date'
            })
            setTimeout(() => {
                const diffMins = MinutesPassed(date);
                if (diffMins < 15) {
                    Alert.alert(
                        language.alert,
                        language.past_booking_error,
                        [

                            { text: "OK", onPress: () => { } }
                        ],
                        { cancelable: true }
                    );
                } else {
                    dispatch(getEstimate({
                        bookLater: true,
                        bookingDate: date,
                        pickup: {coords: {lat:tripdata.pickup.lat, lng:tripdata.pickup.lng} , description: tripdata.pickup.add},
                        drop:  {coords: {lat:tripdata.drop.lat, lng:tripdata.drop.lng}, description: tripdata.drop.add},
                        carDetails: tripdata.carType,
                    }));
                }
            }, 1000);
        }
    };

    const LoadingModalBody = () => {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={loadingModal}
                onRequestClose={() => {
                    setLoadingModal(false);
                }}
                
            >
                <View style={{ flex: 1, backgroundColor: "rgba(22,22,22,0.8)", justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ width: '85%', backgroundColor: colors.GREY.Smoke_Grey, borderRadius: 10, flex: 1, maxHeight: 70 }}>
                        <View style={{ alignItems: 'center', flexDirection: 'row', flex: 1, justifyContent: "center" }}>
                            <Image
                                style={{ width: 80, height: 80, backgroundColor: colors.TRANSPARENT }}
                                source={require('../../assets/images/loader.gif')}
                            />
                            <View style={{ flex: 1 }}>
                                <Text style={{ color: colors.BLACK, fontSize: 16, }}>{language.driver_finding_alert}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    return (
        <View style={styles.mainViewStyle}>
            <NavigationEvents
                onWillFocus={payload => {
                }}
                onDidFocus={payload => {
                    pageActive.current = true;
                }}
                onWillBlur={payload => {
                    pageActive.current = false;
                }}
                onDidBlur={payload => {
                }}
            />
            <Header
                backgroundColor={colors.GREY.default}
                leftComponent={{ icon: 'align-left', type: 'feather', color: '#8468fa', size: 20, component: TouchableWithoutFeedback, onPress: () => { props.navigation.toggleDrawer(); } }}
                centerComponent={<Text style={styles.headerTitleStyle}>{language.map_screen_title}</Text>}
                containerStyle={styles.headerStyle}
                innerContainerStyles={styles.headerInnerStyle}
            />

            <View style={styles.myViewStyle}>
                
                
                <View style={styles.iconsViewStyle}>
                    <TouchableOpacity onPress={() => tapAddress('pickup')} style={styles.contentStyle}>
                        <View style={styles.textIconStyle}>
                            <Text numberOfLines={1} style={[styles.textStyle, tripdata.selected == 'pickup' ? { fontSize: 14 } : { fontSize: 14 }]}>{tripdata.pickup && tripdata.pickup.add ? tripdata.pickup.add : language.map_screen_where_input_text}</Text>
                            <Icon
                                name='gps-fixed'
                                color={'#58b950'}
                                size={tripdata.selected == 'pickup' ? 14 : 14}
                                containerStyle={{ flex: 1 }}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => tapAddress('drop')} style={styles.contentStyle}>
                        <View style={styles.textIconStyle}>
                            <Text numberOfLines={1} style={[styles.textStyle, tripdata.selected == 'drop' ? { fontSize: 14 } : { fontSize: 14 }]}>{tripdata.drop && tripdata.drop.add ? tripdata.drop.add : language.map_screen_drop_input_text}</Text>
                            <Icon
                                name='map-pin'
                                type='feather'
                                color={'red'}
                                size={tripdata.selected == 'drop' ? 14 : 14}
                                containerStyle={{ flex: 1 }}
                            />
                        </View>
                    </TouchableOpacity>

                </View>
            </View>
            <View style={styles.mapcontainer}>
           
                {region && tripdata && tripdata.pickup?
                    <MapComponent
                        markerRef={marker => { marker = marker; }}
                        mapStyle={styles.map}
                        mapRegion={region}
                        nearby={freeCars}
                        onRegionChangeComplete={onRegionChangeComplete}
                        onPanDrag={onPanDrag}
                    />
                : null}
                
               

                {tripdata.selected == 'pickup' ?
                    <View pointerEvents="none" style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent' }}>
                        <Image pointerEvents="none" style={{ marginBottom: 40, height: 40, resizeMode: "contain" }} source={require('../../assets/images/green_pin.png')} />
                    </View>
                    :
                    <View pointerEvents="none" style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent' }}>
                        <Image pointerEvents="none" style={{ marginBottom: 40, height: 40, resizeMode: "contain" }} source={require('../../assets/images/rsz_2red_pin.png')} />
                    </View>
                }
                <View  
                    
                    style={{ 
                        position: 'absolute', 
                        height: Platform.OS == 'ios'?55:42, 
                        width: Platform.OS == 'ios'?55:42, 
                        bottom: 51, 
                        right: 11, 

                        backgroundColor: '#fff', 
                        borderRadius: Platform.OS == 'ios'?30:3, 
                        elevation: 2,
                        shadowOpacity: 0.3,
                        shadowRadius: 3,
                        shadowOffset: {
                            height: 0,
                            width: 0
                        },
                    }}
                >
                    <TouchableOpacity onPress={locateUser}
                        style={{ 
                            height: Platform.OS == 'ios'?55:42, 
                            width: Platform.OS == 'ios'?55:42,
                                alignItems: 'center', 
                            justifyContent: 'center', 
                        }}
                    >
                        <Icon
                            name='gps-fixed'
                            color={"#666699"}
                            size={26}
                        />

                    </TouchableOpacity>


                   
                    
                </View>

                
            </View>
            { activeBookings && activeBookings.length>=1?
            <View style={styles.compViewStyle}>
                <ScrollView horizontal={true} pagingEnabled={true} showsHorizontalScrollIndicator={false}>
                    {activeBookings.map((booking, key) => {
                        return (
                        <TouchableWithoutFeedback key={key} style={styles.activeBookingItem} onPress={() => {props.navigation.navigate('BookedCab',{bookingId:booking.id})}}>
                            <Image style={{marginLeft:5,width: 22, height: 22}} source={{ uri: booking.carImage }} resizeMode={'contain'}  />
                        <Text style={{marginLeft:5,width: 118, color:'white', fontFamily:'Roboto-Bold', fontSize:12}}>{language.active_booking}</Text>
                            <Text style={{marginLeft:10, width: width - 180,marginRight:10,color:'white', fontSize: 12}} numberOfLines={1} ellipsizeMode='tail'>{booking.drop.add}</Text>           
                        </TouchableWithoutFeedback>
                        );
                    })}
                </ScrollView>
            </View>
            :null}
            <View style={styles.compViewStyle2}>
                {/* <Text style={styles.sampleTextStyle}>{language.cab_selection_subtitle}</Text>
                <ScrollView horizontal={true} style={styles.adjustViewStyle} showsHorizontalScrollIndicator={true}>
                    {allCarTypes.map((prop, key) => {
                        return (
                            <View key={key} style={styles.cabDivStyle} >
                                <TouchableOpacity onPress={() => { selectCarType(prop, key) }} style={[styles.imageStyle, {
                                    backgroundColor: prop.active == true ? colors.YELLOW.secondary : colors.WHITE
                                }]
                                }>
                                    <Image resizeMode="contain" source={prop.image ? { uri: prop.image } : require('../../assets/images/microBlackCar.png')} style={styles.imageStyle1} />
                                </TouchableOpacity>
                                <View style={styles.textViewStyle}>
                                    <Text style={styles.text1}>{prop.name.toUpperCase()}</Text>
                                    <View style={{flexDirection:'row',alignItems:'center'}}> 
                                        <Text style={styles.text2}>{prop.minTime != '' ? prop.minTime : language.not_available}</Text>
                                        {
                                        prop.extra_info && prop.extra_info !=''?
                                            <Tooltip style={{marginLeft:3, marginRight:3}}
                                                backgroundColor={"#fff"}
                                                overlayColor={'rgba(50, 50, 50, 0.70)'}
                                                height={10 + 30 * (prop.extra_info.split(',').length)}
                                                width={180}
                                                popover={
                                                    <View style={{ justifyContent:'space-around', flexDirection:'column'}}>
                                                        {
                                                        prop.extra_info.split(',').map((ln)=> <Text key={ln} style={{margin:5}}>{ln}</Text> )
                                                        }
                                                    </View>
                                                }>
                                                <Icon
                                                    name='information-circle-outline'
                                                    type='ionicon'
                                                    color='#517fa4'
                                                    size={28}
                                                />
                                            </Tooltip>
                                        :null}
                                    </View>
                                   
                                </View>
                            </View>

                        );
                    })}
                </ScrollView> */}

                
                <View style={{flexDirection: 'row', justifyContent:'center', margin:10, marginBottom:20,}}>

                    <View
                    style={{flexDirection:'row', flex:1, alignSelf:'center', marginLeft:20,}}
                    >
                        
                        <TouchableOpacity onPress={onPressBookLater}
                        
                        style={styles.laterCircle}
                        >
                        <Icon
                            name='event-note'
                            color={"#af31f8"}
                            size={22}
                        />
                    </TouchableOpacity>

                    </View>

                    
                       
                    {/* Boook Now Gradient Button  */}
                       <LinearGradient
                       
                        colors={['#38ccfc', '#b329ff']}
                        start={[0,1]}
                        end={[1,0]}
                        style={styles.bookNowBtn}
                        >
                
                            <TouchableOpacity onPress={onPressBook}
                                style={{ 
                                    height: Platform.OS == 'ios'?45:45, 
                                        alignItems: 'center', 
                                    justifyContent: 'center', 
                                }}
                            >
                                <Text style={{ color: colors.WHITE, fontFamily: 'Roboto-Bold', fontSize: 16, }}>{language.book_now_button}</Text>
                            </TouchableOpacity>
                     </LinearGradient>


                  

                </View>






                

            </View>


            



            {
                LoadingModalBody()
            }
            <DateTimePickerModal
                date={pickerConfig.selectedDateTime}
                minimumDate={new Date()}
                isVisible={pickerConfig.dateModalOpen}
                mode={pickerConfig.dateMode}
                onConfirm={handleDateConfirm}
                onCancel={hideDatePicker}
            />
            <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>This app is collecting data permission for location to enable you (Pickup location, Drop destination, Booking)</Text>
            <Text style={styles.modalText}>Please be informed that the 'location' for our app is used</Text>
            <Text style={styles.modalText}>- For Drivers app will continue run in  'background' even when 'the app is closed' it will be always in use  to receive new rides</Text>
            <Text style={styles.modalText}>- For customers might be your location  is still running in the background to live track your full trip details</Text>
            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: '#2196F3' }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}>
              <Text style={styles.textStyleModal}>Ok,I understand</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
        </View>
    );

}

const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: colors.WHITE,
        borderBottomWidth: 0
    },
    headerInnerStyle: {
        marginLeft: 10,
        marginRight: 10
    },
    headerTitleStyle: {
        color: '#8468fa',
        fontFamily: 'Roboto-Bold',
        fontSize: 15
    },
    mapcontainer: {
        flex: 6,
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    map: {
        flex: 1,
        ...StyleSheet.absoluteFillObject,
    },
    mainViewStyle: {
        flex: 1,
        backgroundColor: colors.WHITE,
    },
    myViewStyle: {
        flex: 1.2,
        flexDirection: 'row',
        borderTopWidth: 0,
        alignItems: 'center',
        backgroundColor: colors.WHITE,
        paddingHorizontal: 15,
    },
    coverViewStyle: {
        flex: 3.5,
        alignItems: 'center'
    },
    viewStyle1: {
        height: 12,
        width: 12,
        borderRadius: 15 / 2,
        backgroundColor: colors.YELLOW.light
    },
    viewStyle2: {
        height: height / 25,
        width: 1,
        backgroundColor: colors.YELLOW.light
    },
    viewStyle3: {
        height: 14,
        width: 14,
        backgroundColor: colors.GREY.iconPrimary
    },
    iconsViewStyle: {
        flex: 9,
        justifyContent: 'center',
        marginBottom: 10,
    },
    contentStyle: {
        justifyContent: 'center',
        borderBottomColor: colors.WHITE,
        borderBottomWidth: 1,
        backgroundColor: '#eff8ff',
        borderRadius: 20,
        paddingHorizontal:20,
        marginTop:6,
    },
    textIconStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row-reverse'
    },
    textStyle: {
        flex: 9,
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        fontWeight: '400',
        color: '#2b2b2b',
        marginTop: 10,
        marginBottom: 10
    },
    searchClickStyle: {
        //flex: 1, 
        justifyContent: 'center'
    },
    compViewStyle: {
        flex: 0.5,
        backgroundColor: '#5d80f8',
        shadowColor: colors.BLACK,
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
    },
    activeBookingItem:{
        flex:1,
        flexGrow:1,
        flexDirection:'row',
        width:width,
        alignItems:'center',
        justifyContent:'flex-start'
    },
    compViewStyle2: {
        flex: 0.8,
        alignItems: 'center'
    },
    pickCabStyle: {
        flex: 0.3,
        fontFamily: 'Roboto-Bold',
        fontSize: 15,
        fontWeight: '500',
        color: colors.BLACK
    },
    sampleTextStyle: {
        flex: 0.2,
        fontFamily: 'Roboto-Bold',
        fontSize: 13,
        fontWeight: '300',
        color: colors.GREY.secondary,
        marginTop:5
    },
    adjustViewStyle: {
        flex: 9,
        flexDirection: 'row',
        //justifyContent: 'space-around',
        marginTop: 8
    },
    cabDivStyle: {
        flex: 1,
        width: width / 3,
        alignItems: 'center'
    },
    imageViewStyle: {
        flex: 2.7,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    imageStyle: {
        height: height / 14,
        width: height / 14,
        borderRadius: height / 14 / 2,
        borderWidth: 3,
        borderColor: colors.YELLOW.secondary,
        //backgroundColor: colors.WHITE, 
        justifyContent: 'center',
        alignItems: 'center'
    },
    textViewStyle: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    text1: {

        fontFamily: 'Roboto-Bold',
        fontSize: 14,
        fontWeight: '900',
        color: colors.BLACK
    },
    text2: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        fontWeight: '900',
        color: colors.GREY.secondary
    },
    imagePosition: {
        height: height / 14,
        width: height / 14,
        borderRadius: height / 14 / 2,
        borderWidth: 3,
        borderColor: colors.YELLOW.secondary,
        //backgroundColor: colors.YELLOW.secondary, 
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageStyleView: {
        height: height / 14,
        width: height / 14,
        borderRadius: height / 14 / 2,
        borderWidth: 3,
        borderColor: colors.YELLOW.secondary,
        //backgroundColor: colors.WHITE, 
        justifyContent: 'center',
        alignItems: 'center'
    },    
    imageStyle1: {
        height: height / 20.5,
        width: height / 20.5
    },
    imageStyle2: {
        height: height / 20.5,
        width: height / 20.5
    },
    buttonContainer: {
        flex: 1
    },

    buttonTitleText: {
        color: colors.GREY.default,
        fontFamily: 'Roboto-Regular',
        fontSize: 20,
        alignSelf: 'flex-end'
    },

    cancelButtonStyle: {
        backgroundColor: colors.GREY.whiteish,
        elevation: 0,
        width: "60%",
        borderRadius: 5,
        alignSelf: "center"
    },

    laterCircle: {
         backgroundColor: '#fff', 
        justifyContent: 'center',
        height: 45,
        width: 45,
        borderRadius: Platform.OS == 'ios'?30:30, 
        elevation: 2,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: {
            height: 0,
            width: 0
        }
    },

    bookNowBtn: {
        flexDirection:'row', 
        flex:4, 
        alignSelf:'center', 
        justifyContent:'center',
        marginRight:20, 
        borderRadius: Platform.OS == 'ios'?22:22, 
    },
    /*modal style */
    /*modal popup styles*/
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
      },
      modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
      },
      openButton: {
        backgroundColor: '#F194FF',
        borderRadius: 20,
        padding: 10,
        elevation: 2,
      },
      textStyleModal: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
      },
      modalText: {
        marginBottom: 15,
        textAlign: 'center',
      },
    
});