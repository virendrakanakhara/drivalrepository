import React, { useState, useRef, useEffect, useContext } from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    TouchableOpacity,
    Text,
    TextInput,
    Platform,
    TouchableWithoutFeedback, Alert,Modal
} from 'react-native';
import { Icon, Button, Header } from 'react-native-elements';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { LinearGradient } from 'expo-linear-gradient';
import { colors } from '../common/theme';
var { width, height } = Dimensions.get('window');
import { useSelector, useDispatch } from 'react-redux';
import { language } from 'config';
import { FirebaseContext } from 'common/src';
import { PromoComp } from "../components";

export default function FareScreen(props) {
    const { api } = useContext(FirebaseContext);
    const {
        addBooking,
        clearEstimate,
        clearBooking,
        clearTripPoints,
        editPromo,
        editPromodata
        
    } = api;
    const dispatch = useDispatch();
    const userdata = useSelector(state => state.auth.info.profile);
    const uid = useSelector(state => state.auth.info.uid);
    const settings = useSelector(state => state.settingsdata.settings);
    const tripdata = useSelector(state => state.tripdata);
    const auth = useSelector(state => state.auth);
    const estimate = useSelector(state => state.estimatedata.estimate);
    const bookingdata = useSelector(state => state.bookingdata);
    const [userpromoinputvalue,setUserpromoinputvalue] = useState("");
    const promos = useSelector(state => state.promodata.promos);
    const mapRef = useRef(null);

    const [promodalVisible, setPromodalVisible] = useState(false);
    const [buttonDisabled, setButtonDisabled] = useState(false);
    const estiamtefare = props.navigation.getParam('estimatefare');
    const [discountDetails, setDiscountDetails] = useState({
        amount: estiamtefare,
        discount: 0,
        promo_applied: false,
        promo_details: null,
        payableAmmount: estiamtefare,
        uid:uid,
        id:uid
        
      });

    const onPressCancel = () => {
        removePromo();
        dispatch(clearEstimate());
        dispatch(clearTripPoints());
        setButtonDisabled(false);
        props.navigation.goBack();
    }

    useEffect(() => {
        setTimeout(() => {
            mapRef.current.fitToCoordinates([{ latitude: tripdata.pickup.lat, longitude: tripdata.pickup.lng }, { latitude: tripdata.drop.lat, longitude: tripdata.drop.lng }], {
                edgePadding: { top: 40, right: 40, bottom: 40, left: 40 },
                animated: true,
            });
        }, 1000);
    }, []);

    useEffect(() => {
        if (bookingdata.booking) {
            dispatch(clearEstimate());
            dispatch(clearBooking());
            dispatch(clearTripPoints());
            if(bookingdata.booking.mainData.bookLater){
                props.navigation.navigate('RideList');
            }else{
                props.navigation.navigate('BookedCab',{bookingId:bookingdata.booking.booking_id});
            }
        }
        if (bookingdata.error && bookingdata.error.flag) {
            Alert.alert(bookingdata.error.msg);
            dispatch(clearBooking());
        }
    }, [bookingdata.booking, bookingdata.error, bookingdata.error.flag]);

    const bookNow = () => {
        if(auth.info.profile.mobile == '' || auth.info.profile.mobile == ' ' || !auth.info.profile.mobile){
            Alert.alert(language.alert, language.updatemobile);
        }else{
            setButtonDisabled(true);
            dispatch(addBooking({
                pickup: estimate.pickup,
                drop: estimate.drop,
                carDetails: estimate.carDetails,
                userDetails: auth.info,
                estimate: estimate,
                tripdate: estimate.bookLater ? new Date(estimate.bookingDate).toString() : new Date().toString(),
                bookLater: estimate.bookLater,
                settings: settings,
                booking_type_web: false
            }));
        }
    };


    const promoModal = () => {
        return (
          <Modal
            animationType="none"
            visible={promodalVisible}
            onRequestClose={() => {
              setPromodalVisible(false);
            }}>
            <Header
              backgroundColor={colors.GREY.default}
              rightComponent={{ icon: 'ios-close', type: 'ionicon', color: colors.WHITE, size: 45, component: TouchableWithoutFeedback, onPress: () => { setPromodalVisible(false) } }}
              centerComponent={<Text style={styles.headerTitleStyle}>{language.your_promo}</Text>}
              containerStyle={styles.headerStyle}
              innerContainerStyles={{ marginLeft: 10, marginRight: 10 }}
            />
            <PromoComp onPressButton={(item, index) => { selectCoupon(item, index) }}></PromoComp>
          </Modal>
        )
      }

      const openPromoModal = () => {
        
       
        
         let flag=false;
        
        promos.map((item)=>{
           
             if(item.promo_name == userpromoinputvalue)
            {
                flag=true;

                let data = { ...discountDetails };
                data.payableAmmount = data.amount;
                data.discount = 0;
                data.promo_applied = false;
                data.promo_details = null;
        
        
    
        setDiscountDetails(data);
        //dispatch(editPromodata(data));

                selectCoupon(item);
            }


        });
        if(flag)
        {
    //         //selectCoupon()
        }
        else
        {
             alert('Invalid Promo Code !!');
        }  
       /* setPromodalVisible(!promodalVisible);
        let data = { ...discountDetails };
        data.payableAmmount = data.amount;
        data.discount = 0;
        data.promo_applied = false;
        data.promo_details = null;
        
        
       
        setDiscountDetails(data);
        dispatch(editPromodata(data));*/
       
      }
    
      const removePromo = () => {
        let data = { ...discountDetails };
        if(data.promo_details!=null)
        {
        data.promo_details.user_avail = parseInt(data.promo_details.user_avail) - 1;
        delete data.promo_details.usersUsed[uid];
        dispatch(editPromo(data.promo_details));
        
        data.discount = 0;
        data.promo_applied = false;
        data.promo_details = null;
        data.payableAmmount = data.amount;
        setDiscountDetails(data);
        
        //setUseWalletCash(false);
        }
        dispatch(editPromodata(data,"Delete"));
      }

      const selectCoupon = (item, index) => {
        var toDay = new Date();
        var expDate = new Date(item.promo_validity)
        expDate.setDate(expDate.getDate() + 1);
        item.usersUsed = item.usersUsed? item.usersUsed :{};
         if (item.user_avail && item.user_avail >= item.promo_usage_limit) {
          Alert.alert(language.alert,language.promo_exp_limit)
        } else if (item.usersUsed[uid]) {
          Alert.alert(language.alert,language.promo_used)
        } else if (toDay > expDate) {
          Alert.alert(language.alert,language.promo_exp)
        } else {
          let discounttype = item.promo_discount_type.toUpperCase();
          if (discounttype == 'PERCENTAGE') {
            let discount = parseFloat(discountDetails.amount * item.promo_discount_value / 100).toFixed(2);
            if (discount > item.max_promo_discount_value) {
              let discount = item.max_promo_discount_value;
              let data = { ...discountDetails };
              data.discount = discount
              data.promo_applied = true
              item.user_avail = item.user_avail? parseInt(item.user_avail) + 1 : 1;
              item.usersUsed[uid]=true;
              data.payableAmmount = parseFloat(data.payableAmmount - discount).toFixed(2);
              dispatch(editPromo(item));
              data.promo_details = item
              
              setDiscountDetails(data);
              dispatch(editPromodata(data));
              setPromodalVisible(false);
            } else {
              let data = { ...discountDetails };
              data.discount = discount
              data.promo_applied = true
              item.user_avail = item.user_avail? parseInt(item.user_avail) + 1 : 1;
              item.usersUsed[uid]=true;
              dispatch(editPromo(item));
              data.promo_details = item,
              data.payableAmmount = parseFloat(data.payableAmmount - discount).toFixed(2);
              setDiscountDetails(data);
              dispatch(editPromodata(data));
              setPromodalVisible(false);
            }
          } else {
            let discount = item.max_promo_discount_value;
            let data = { ...discountDetails };
            data.discount = discount
            data.promo_applied = true
            item.user_avail = item.user_avail? parseInt(item.user_avail) + 1 : 1;
            item.usersUsed[uid]=true;
            dispatch(editPromo(item));
            data.promo_details = item,
            data.payableAmmount = parseFloat(data.payableAmmount - discount).toFixed(2);
            setDiscountDetails(data);
            dispatch(editPromodata(data));
            setPromodalVisible(false);
          }
        }
    
      }

    return (
        <View style={styles.container}>
            <Header
                backgroundColor={colors.GREY.default}
                leftComponent={{ icon: 'align-left', type: 'feather', color: '#8468fa', size: 20, component: TouchableWithoutFeedback, onPress: () => { props.navigation.toggleDrawer(); } }}
                centerComponent={<Text style={styles.headerTitleStyle}>{language.confirm_booking}</Text>}
                containerStyle={styles.headerStyle}
                innerContainerStyles={styles.headerInnerStyle}
            />

            <View style={styles.topContainer}>
              
                <View style={styles.topRightContainer}>
                    <TouchableOpacity style={styles.whereButton}>
                        <View style={styles.whereContainer}>
                            <Text numberOfLines={1} style={styles.whereText}>{tripdata.pickup.add}</Text>
                            <Icon
                                name='gps-fixed'
                                color={'#58b950'}
                                size={14}
                                containerStyle={styles.iconContainer}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.dropButton}>
                        <View style={styles.whereContainer}>
                            <Text numberOfLines={1} style={styles.whereText}>{tripdata.drop.add}</Text>
                            <Icon
                                name='map-pin'
                                type='feather'
                                color={'red'}
                                size={14}
                                containerStyle={styles.iconContainer}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.mapcontainer}>
                {tripdata && tripdata.pickup.lat ?
                    <MapView
                        ref={mapRef}
                        style={styles.map}
                        provider={PROVIDER_GOOGLE}
                        initialRegion={{
                            latitude: (tripdata.pickup.lat),
                            longitude: (tripdata.pickup.lng),
                            latitudeDelta: 0.9922,
                            longitudeDelta: 1.9421
                        }}
                    >
                        <Marker
                            coordinate={{ latitude: (tripdata.pickup.lat), longitude: (tripdata.pickup.lng) }}
                            title={tripdata.pickup.add}
                            pinColor={colors.GREEN.default}
                        >
                        </Marker>


                        <Marker
                            coordinate={{ latitude: (tripdata.drop.lat), longitude: (tripdata.drop.lng) }}
                            title={tripdata.drop.add}
                        >
                        </Marker>

                        {estimate && estimate.waypoints ?
                            <MapView.Polyline
                                coordinates={estimate.waypoints}
                                strokeWidth={4}
                                strokeColor={colors.BLUE.default}
                            />
                            : null}

                    </MapView>
                    : null}
            </View>
            <View style={styles.bottomContainer}>
                <View style={styles.offerContainer}>
                    <View style={{flex:1}}>
                    <TouchableOpacity >
                        <Text style={styles.offerText}> {language.estimate_fare_text}</Text>
                    </TouchableOpacity>
                    </View>
                    
                    {userdata && userdata.usertype == 'rider' ? discountDetails.promo_applied ?
                    <View style={{flex:2.5,height:35}}>
                    <View style={{flex:1,height:35,flexDirection:"row",padding:2}}>  
                    <Text
             style={{width:"73%",height:35,marginRight:0,color:"red",textAlignVertical:"center", padding: 8}}
            
             
           >Promo Code : {userpromoinputvalue}</Text>    
                   <TouchableOpacity style={{width:"25%",backgroundColor:"blue"}}
                     onPress={() => { removePromo() }}>
                     <Text style={{ color: colors.WHITE,   height:35,width:"100%",textAlignVertical:"center",textAlign:"center",padding:8, fontSize: 14, fontWeight: '500',backgroundColor:"red" }}>{language.remove_promo}</Text>
                   </TouchableOpacity></View>
                   </View>
              :
              <View style={{flex:3,height:40}}>
               <View style={{flex:1,height:30,flexDirection:"row",padding:2}}>  
               <TextInput
        style={{width:"73%",height:35,marginRight:2, backgroundColor: colors.WHITE, textAlign:'center'}}
        value={userpromoinputvalue}
        onChangeText={setUserpromoinputvalue}
        
        placeholder="Enter Promo Code"
        
      />    
              <TouchableOpacity style={{width:"25%"}}
                onPress={() => { openPromoModal() }}>
                <Text style={{ color: colors.WHITE,width:"100%",textAlign:"center", alignContent: 'center',alignSelf:'center',paddingRight:5, fontSize: 14, fontWeight: '700',backgroundColor:"#af31f8", height:35, padding:8}}>{language.apply_promo}</Text>
              </TouchableOpacity></View>
              </View>
              : null}
                </View>
                
            {/************* BOX CALCULATION SECTION   **************/}
                <View style={styles.boxesSection}>

                    {/* Estimate Fare*/}
                    <View style={{flex:1}}>
                            <Text style={styles.boxesLabel}>{language.total_fare}</Text>
                    
                        <View>
                            <Text style={styles.boxesCost}>{settings ? settings.symbol : null} {estimate ? estimate.estimateFare : null}</Text>
                         </View>

                    </View>

                    
                    {/* Discount*/}
                    <View style={{flex:1}}>
                            <Text style={styles.boxesLabel}>Discount</Text>
                    
                        <View>
                            <Text style={styles.boxesCost}>{settings ? settings.symbol : null} {discountDetails ? discountDetails.discount : null}</Text>
                         </View>

                    </View>

                    
                    {/* Distance */}
                    <View style={{flex:1,}}>
                            <Text style={styles.boxesLabel}>Distance</Text>
                    
                        <View style={{borderRadius:20,}}>
                            <Text style={styles.boxesCost}>{estimate && estimate.estimateDistance ? parseFloat(estimate.estimateDistance).toFixed(2) : 0} {settings.convert_to_mile? language.mile : language.km} </Text>
                         </View>

                    </View>




                    {/* Arrival Time */}
                    <View style={{flex:1}}>
                            <Text style={styles.boxesLabel}>Arrival</Text>
                    
                        <View>
                            <Text style={styles.boxesCost}>{estimate ? parseFloat(estimate.estimateTime/60).toFixed(0): 0} {language.mins}</Text>
                         </View>

                    </View>


                   
                </View>
                    


                




                {/* CONFIRM OR CANCEL SECTION */}
                <View style={{flexDirection: 'row', justifyContent:'center', margin:12 }}>
                    
                    <View
                    style={{flexDirection:'row', flex:1, alignSelf:'center', marginLeft:20,}}
                    >
                        
                        <TouchableOpacity onPress={onPressCancel}
                        
                        style={styles.laterCircle}
                        >
                        <Icon
                            name='cancel'
                            color={"#af31f8"}
                            size={45}
                        />
                    </TouchableOpacity>

                    </View>
                    

                    <LinearGradient
                       
                        colors={['#38ccfc', '#b329ff']}
                        start={[0,1]}
                        end={[1,0]}
                        style={styles.bookNowBtn}
                        >
                
                            <TouchableOpacity onPress={bookNow}
                                style={{ 
                                    height: Platform.OS == 'ios'?45:45, 
                                        alignItems: 'center', 
                                    justifyContent: 'center', 
                                }}
                            >
                                <Text style={{ color: colors.WHITE, fontFamily: 'Roboto-Bold', fontSize: 16, }}>{language.confirm_booking}</Text>
                            </TouchableOpacity>
                     </LinearGradient>

                </View>
         </View>
            {
        promoModal()
      }
        </View>
    );

}

const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: colors.WHITE,
        borderBottomWidth: 0
    },
    headerInnerStyle: {
        marginLeft: 10,
        marginRight: 10
    },
    headerTitleStyle: {
        color: '#8468fa',
        fontFamily: 'Roboto-Bold',
        fontSize: 15
    },
    container: {
        flex: 1,
        backgroundColor: colors.WHITE,
        //marginTop: StatusBar.currentHeight
    },
    topContainer: {
        flex: 1.2,
        flexDirection: 'row',
        borderTopWidth: 0,
        alignItems: 'center',
        backgroundColor: colors.WHITE,
        paddingHorizontal: 15,
    },
    topLeftContainer: {
        flex: 1.5,
        alignItems: 'center'
    },
    topRightContainer: {
        flex: 9.5,
        justifyContent: 'space-between',
    },
    circle: {
        height: 12,
        width: 12,
        borderRadius: 15 / 2,
        backgroundColor: colors.YELLOW.light
    },
    staightLine: {
        height: height / 25,
        width: 1,
        backgroundColor: colors.YELLOW.light
    },
    square: {
        height: 14,
        width: 14,
        backgroundColor: colors.GREY.iconPrimary
    },
    whereButton: { 
        flex: 1, 
        justifyContent: 'center', 
        borderBottomColor: colors.WHITE, 
        borderBottomWidth: 1,
        backgroundColor: '#eff8ff',
        borderRadius: 20,
        paddingHorizontal:20,
        marginTop:6,
     },
    whereContainer: { flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' },
    whereText: { flex: 9, fontFamily: 'Roboto-Regular', fontSize: 14, fontWeight: '400', color: colors.GREY.default },
    iconContainer: { flex: 1 },
    dropButton: { 
        flex: 1, 
        justifyContent: 'center',
        backgroundColor: '#eff8ff',
        borderRadius: 20,
        paddingHorizontal:20,
        marginTop:2,
        marginBottom: 6,
     },
    mapcontainer: {
        flex: 7,
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    map: {
        flex: 1,
        ...StyleSheet.absoluteFillObject,
    },
    bottomContainer: { flex: 2.5, alignItems: 'center' },


    offerContainer: { 
        flex: 2,
        flexDirection:"row", 
        alignContent:'center',
        alignItems: 'center',
        backgroundColor: '#eff8ff', 
        width: width, 
        justifyContent: 'center',
        },
    offerText: { alignSelf: 'center', color: colors.GREY.default, fontSize: 14, fontFamily: 'Roboto-Regular',width:"100%",textAlignVertical:"center", marginLeft:10,},
    priceDetailsContainer: { flex: 2.4, backgroundColor: colors.WHITE, flexDirection: 'row', position: 'relative', zIndex: 1 },
    priceDetailsLeft: { flex: 1 },
    priceDetailsMiddle: { flex: 1 },
    priceDetails: { flex: 1, flexDirection: 'row' },
    totalFareContainer: { flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' },
    totalFareText: { color: colors.GREY.btnPrimary, fontFamily: 'Roboto-Bold', fontSize: 10, textAlign:'center' },
    infoIcon: { flex: 2, alignItems: 'center', justifyContent: 'center' },
    priceText: { alignSelf: 'center', color: colors.GREY.iconSecondary, fontFamily: 'Roboto-Bold', fontSize: 20 },
    triangle: {
        width: 0,
        height: 0,
        backgroundColor: colors.TRANSPARENT,
        borderStyle: 'solid',
        borderLeftWidth: 9,
        borderRightWidth: 9,
        borderBottomWidth: 10,
        borderLeftColor: colors.TRANSPARENT,
        borderRightColor: colors.TRANSPARENT,
        borderBottomColor: colors.YELLOW.secondary,
        transform: [
            { rotate: '180deg' }
        ],
        marginTop: -1, overflow: 'visible'
    },
    lineHorizontal: { height: height / 18, width: 1, backgroundColor: colors.BLACK, alignItems: 'center', marginTop: 10 },
    logoContainer: { flex: 19, alignItems: 'center', justifyContent: 'center' },
    logoImage: { width: 80, height: 80 },
    buttonsContainer: { flex: 1.5, flexDirection: 'row' },
    buttonText: { color: colors.WHITE, fontFamily: 'Roboto-Bold', fontSize: 17, alignSelf: 'flex-end' },
    buttonStyle: { backgroundColor: colors.GREY.secondary, elevation: 0 },
    buttonContainerStyle: { flex: 1, backgroundColor: colors.GREY.secondary },
    confirmButtonStyle: { backgroundColor: colors.GREY.btnPrimary, elevation: 0 },
    confirmButtonContainerStyle: { flex: 1, backgroundColor: colors.GREY.btnPrimary },

    flexView: {
        flex: 1
    },

    cancelButtonStyle: {
        backgroundColor: colors.GREY.whiteish,
        elevation: 0,
        width: "60%",
        borderRadius: 5,
        alignSelf: "center"
    },

    laterCircle: {
         backgroundColor: '#fff', 
        justifyContent: 'center',
        height: 45,
        width: 45,
        borderRadius: Platform.OS == 'ios'?30:30, 
        elevation: 2,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: {
            height: 0,
            width: 0
        }
    },

    bookNowBtn: {
        flexDirection:'row', 
        flex:4, 
        alignSelf:'center', 
        justifyContent:'center',
        marginRight:20, 
        borderRadius: Platform.OS == 'ios'?22:22, 
    },

    boxesSection: {
        flex:2, 
        flexDirection:'row', 
        justifyContent:'space-around', 
        alignItems:'center', 
        alignContent:'center', 
        alignSelf:'center', 
        margin:5,
    },
    
    boxesLabel : {
        alignSelf:'center', 
        textAlignVertical:'center', 
        fontSize:10, 
        marginTop: 15,
        marginBottom:3,
        color: '#af31f8'
    },

    boxesCost: {
        alignSelf:'center', 
        textAlignVertical:'center', 
        fontSize:16,
        padding:3,
        borderRadius:20,
    },

    
});
