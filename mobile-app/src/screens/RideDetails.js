import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableWithoutFeedback,
    ImageBackground,
    ScrollView,
    Dimensions,
    Platform
} from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { Header, Rating, Avatar, Button } from 'react-native-elements';
import Dash from 'react-native-dash';
import { colors } from '../common/theme';
var { width } = Dimensions.get('window');
import { language } from 'config';
import { useSelector } from 'react-redux';

export default function RideDetails(props) {

    const paramData = props.navigation.getParam('data');
    const settings = useSelector(state => state.settingsdata.settings);
    const auth = useSelector(state => state.auth);

    const goBack = () => {
        props.navigation.goBack();
    }

    const goToBooking = (id) => {
        props.navigation.replace('BookedCab',{bookingId:id});
    };

    return (
        <View style={styles.mainView}>
            <Header
                backgroundColor={colors.GREY.default}
                leftComponent={{ icon: 'ios-arrow-back', type: 'ionicon', color: '#af31f8', size: 25, component: TouchableWithoutFeedback, onPress: () => { goBack() } }}
                centerComponent={<Text style={styles.headerTitleStyle}>{language.ride_details_page_title}</Text>}
                containerStyle={styles.headerStyle}
                innerContainerStyles={{ marginLeft: 10, marginRight: 10 }}
            />
            <ScrollView>
                <View style={styles.mapView}>
                    <View style={styles.mapcontainer}>
                        {paramData?
                        <MapView style={styles.map}
                            provider={PROVIDER_GOOGLE}
                            region={{
                                latitude: (paramData.pickup.lat),
                                longitude: (paramData.pickup.lng),
                                latitudeDelta: 0.9922,
                                longitudeDelta: 1.9421
                            }}
                        >
                                <Marker
                                    coordinate={{ latitude: paramData ? (paramData.pickup.lat) : 0.00, longitude: paramData ? (paramData.pickup.lng) : 0.00 }}
                                    title={'marker_title_1'}
                                    description={paramData ? paramData.pickup.add : null}
                                    pinColor={colors.GREEN.default}
                                />
                                <Marker
                                    coordinate={{ latitude: (paramData.drop.lat), longitude: (paramData.drop.lng) }}
                                    title={'marker_title_2'}
                                    description={paramData.drop.add}
                                />
                                <MapView.Polyline
                                    coordinates={paramData.coords}
                                    strokeWidth={4}
                                    strokeColor={colors.BLUE.default}
                                />
                        </MapView>
                        :null}
                    </View>
                </View>


                <View style={styles.rideDesc}>
                    
                    <View style={styles.userDesc}>
                        {/* Driver details  */}
                        <View style={{flex:2}}>
                            {/*Start */}
                            <View style={styles.userDesc}>

                        {/* Driver Image */}
                        {paramData ?
                            paramData.driver_image != '' ?
                                <Avatar
                                    size="small"
                                    rounded
                                    source={{ uri: paramData.driver_image }}
                                    activeOpacity={0.7}
                                />
                                : paramData.driver_name != '' ?
                                    <Avatar
                                        size="small"
                                        rounded
                                        source={require('../../assets/images/profilePic.png')}
                                        activeOpacity={0.7}
                                    /> : null



                            : null
                         }
                        {paramData.status == 'CANCELLED' &&
                        <Avatar
                            size="small"
                            rounded
                            source={require('../../assets/images/profilePic.png')}
                            activeOpacity={0.7}
                        />
                        }

                        <View style={styles.userView}>
                            {/*Driver Name */}
                            {paramData && paramData.driver_name != '' && paramData.status != 'CANCELLED' ?  <Text style={styles.personStyle}>{paramData.driver_name}</Text> : null}
                            {paramData.status == 'CANCELLED' &&  <Text style={styles.personStyle}>-NA-</Text>}
                            {paramData && paramData.rating > 0 ?

                                <View style={styles.personTextView}>
                                    {/*My rating to driver */}
                                   
                                    <Rating
                                        showRating
                                        type="star"
                                        fractions={3}
                                        startingValue={parseFloat(paramData.rating)}
                                        readonly
                                        imageSize={12}
                                        style={{ paddingVertical: 3 }}
                                        showRating={false}
                                    />
                                </View>
                                : null}
                        </View>
                    </View>

                            {/* END */}
                        </View>
                    
                    
                    {/* Car details driver  */}
                        <View style={{flex:2}}>
                            {/*Start */}
                            {paramData && paramData.carType ?
                        <View style={[styles.userDesc, styles.avatarView]}>

                            <Avatar
                                size="small"
                                rounded
                                source={paramData.carImage ? { uri: paramData.carImage } : require('../../assets/images/cartop.png')}
                                activeOpacity={0.7}
                            />
                            <View style={styles.userView}>
                                <Text style={styles.carNoStyle}>{paramData.vehicle_number ? paramData.vehicle_number : <Text> No info</Text>}</Text>
                                <Text style={styles.carNoStyleSubText}>{paramData.carType}</Text>
                            </View>
                        </View>

                        : null}
                            {/*END */}
                        </View>
                    {/* Car details driver  */}



                    {/* FARE COST details   */}
                    <View style={styles.userDesc}>
                    {paramData.status != 'CANCELLED' &&
                        <View style={styles.fareView}>
                            <Text style={styles.textStyle}>{settings.symbol}{paramData && paramData.customer_paid ? parseFloat(paramData.customer_paid).toFixed(2) : paramData && paramData.estimate ? paramData.estimate : 0}</Text>
                        </View>
                    }
                    {paramData.status == 'CANCELLED' &&
                        <View style={styles.fareView}>
                            <Text style={styles.textStyle}>{settings.symbol} 0.00</Text>
                        </View>
                    }
                    </View>
                    {/* FARE details driver  */}
                </View>


                    
                        
            {/* Trip start time */}
                <View style={styles.userDesc}>
                    <View style={styles.location}>
                        
                        {paramData && paramData.pickup ?
                            <View style={styles.address}>
                                <View style={styles.redDot} />
                                <Text style={styles.adressStyle}>{paramData.pickup.add}</Text>
                            </View>
                        : null}
                    </View>
                 </View>
             {/* Trip start time */}

             {/* Trip END time */}
             <View style={styles.userDesc}>
                    <View style={styles.location}>
                        
                        {paramData && paramData.drop ?
                                <View style={styles.address}>
                                    <View style={styles.greenDot} />
                                    <Text style={styles.adressStyle}>{paramData.drop.add}</Text>
                                </View>
                        : null}

                    </View>
                 </View>
             {/* Trip END time */}


                   
                </View>

{/*   Trip Time / Distance */}
<View style={styles.rideDesc}>
                    
                    {paramData && ['PENDING','PAID','COMPLETE'].indexOf(paramData.status) != -1 ?
                        <View style={''}>
                            <View style={''}>
                                <Text style={styles.billTitle}>Trip Time/Distance Details</Text>
                            </View>
                            <View style={styles.billOptions}>
                                <View style={styles.billItem}>
                                    <Text style={styles.billName}>Time : </Text>
                                    <Text style={styles.billAmount}>{paramData && paramData.total_trip_time ? paramData.total_trip_time+' Mins':'-NA-'}</Text>
                                </View>
                                <View style={styles.billItem}>
                                    <View>
                                        <Text style={[styles.billName, styles.billText]}>Distance : </Text>
                                        
                                    </View>
                                    <Text style={styles.billAmount}> {paramData && paramData.estimateDistance ? paramData.estimateDistance+' Kms' : '-NA-'}</Text>
    
                                </View>
    
                               
                            </View>
                            
                            
                        </View>
                        : null}
    
                    </View>
                {/* Trip Time / Distance */}

                
            {/*   BILLING FULL ROW DETAIL */}
                <View style={styles.rideDesc}>
                    
                {paramData && ['PENDING','PAID','COMPLETE'].indexOf(paramData.status) != -1 ?
                    <View style={''}>
                        <View style={''}>
                            <Text style={styles.billTitle}>{language.bill_details_title}</Text>
                        </View>
                        <View style={styles.billOptions}>
                            <View style={styles.billItem}>
                                <Text style={styles.billName}>{language.your_trip} : </Text>
                                <Text style={styles.billAmount}>{settings.symbol} {paramData && paramData.trip_cost > 0 ? parseFloat(paramData.trip_cost).toFixed(2) : paramData && paramData.estimate ? parseFloat(paramData.estimate).toFixed(2) : 0}</Text>
                            </View>
                            <View style={styles.billItem}>
                                <View>
                                    <Text style={[styles.billName, styles.billText]}>{language.discount} : </Text>
                                    
                                </View>
                                <Text style={styles.discountAmount}> - {settings.symbol}{paramData && paramData.discount_amount ? parseFloat(paramData.discount_amount).toFixed(2) : 0}</Text>

                            </View>

                            {paramData && paramData.cardPaymentAmount ? paramData.cardPaymentAmount > 0 ?
                                <View style={styles.billItem}>
                                    <View>
                                        <Text >{language.CardPaymentAmount}</Text>

                                    </View>
                                    <Text >  {settings.symbol}{paramData && paramData.cardPaymentAmount ? parseFloat(paramData.cardPaymentAmount).toFixed(2) : 0}</Text>

                                </View>
                                : null : null}
                            {paramData && paramData.cashPaymentAmount ? paramData.cashPaymentAmount > 0 ?
                                <View style={styles.billItem}>
                                    <View>
                                        <Text style={styles.billName}>Cash Payment : </Text>

                                    </View>
                                    <Text style={styles.billAmount}>  {settings.symbol}{paramData && paramData.cashPaymentAmount ? parseFloat(paramData.cashPaymentAmount).toFixed(2) : 0}</Text>

                                </View>
                                : null : null}
                            {paramData && paramData.usedWalletMoney ? paramData.usedWalletMoney > 0 ?
                                <View style={styles.billItem}>
                                    <View>
                                        <Text style={styles.billName}>{language.WalletPayment}</Text>

                                    </View>
                                    <Text style={styles.billAmount}>  {settings.symbol}{paramData && paramData.usedWalletMoney ? parseFloat(paramData.usedWalletMoney).toFixed(2) : 0}</Text>

                                </View>
                                : null : null}
                        </View>
                        
                        
                    </View>
                    : null}

                </View>
            {/* BILLING FULL ROW DETAILS */}



            {/*   Payment Status FULL ROW DETAIL */}
            <View style={styles.rideDesc}>
            {paramData &&  ['PENDING','PAID','COMPLETE'].indexOf(paramData.status) != -1 ?
                    <View>
                        
                        <View style={styles.paybleAmtView}>
                            <Text style={styles.billTitle}>{language.grand_total}</Text>
                            <Text style={styles.grandAmount}>{settings.symbol}{paramData && paramData.customer_paid ? parseFloat(paramData.customer_paid).toFixed(2) : null}</Text>
                        </View>

                        {paramData && paramData.status ?
                            <View style={styles.billOptions}>
                                <View style={styles.billItem}>
                                    <Text style={styles.billName}>{language.payment_status}</Text>
                                    <Text style={styles.billAmount}>{language[paramData.status]}</Text>

                                </View>
                                {['PAID','COMPLETE'].indexOf(paramData.status) != -1 ?
                                <View style={styles.billItem}>
                                    <Text style={styles.billName}>{language.pay_mode}</Text>
                                    <Text style={styles.billAmount}>{paramData.payment_mode ? paramData.payment_mode : null} {paramData.gateway ? '(' + paramData.gateway + ')' : null}</Text>
                                </View>
                                :null}
                            </View>
                            : <View style={styles.billOptions}>
                                <View style={styles.billItem}></View>
                            </View>}
                    </View>
                :null}
            </View>
            {/*   Payment Status FULL ROW DETAIL */}


                
                
                {(paramData && paramData.status &&  auth && auth.info && auth.info.profile && 
                    (((['NEW','ACCEPTED','ARRIVED','STARTED','REACHED','PENDING','PAID'].indexOf(paramData.status) != -1) && auth.info.profile.usertype=='rider') ||
                    ((['ACCEPTED','ARRIVED','STARTED','REACHED'].indexOf(paramData.status) != -1) && auth.info.profile.usertype=='driver')))?
                    <View style={styles.locationView2}>
                        <Button
                            title={language.go_to_booking}
                            loading={false}
                            loadingProps={{ size: "large", color: colors.GREEN.default }}
                            titleStyle={styles.buttonTitleText2}
                            onPress={() => { goToBooking(paramData.id) }}
                            containerStyle={styles.paynowButton}
                        />
                    </View> : null}


            {/*   FINAL FARE BUTTON */}
                <View style={styles.finalFare}>
                    <Text style={{fontSize:15, fontFamily:'Roboto-Regular', color:'black'}}>
                        Final Amount :  <Text style={{fontSize:16, fontFamily:'Roboto-Bold', color: '#af31f8'}} >{settings.symbol}{paramData && paramData.customer_paid ? parseFloat(paramData.customer_paid).toFixed(2) : 0.00}</Text>
                    </Text>
                </View>
            {/*   FINAL FARE BUTTON */}

            </ScrollView>
        </View>
    )

}

const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: colors.WHITE,
        borderBottomWidth: 0
    },
    headerTitleStyle: {
        color: colors.GREY.default,
        fontFamily: 'Roboto-Bold',
        fontSize: 14
    },
    containerView: {
        flex: 1
    },
    textContainer: {
        textAlign: "center"
    },
    mapView: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 160,
        marginBottom: 8
    },
    mapcontainer: {
        flex: 7,
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    map: {
        flex: 1,
        ...StyleSheet.absoluteFillObject,
    },
    triangle: {
        width: 0,
        height: 0,
        backgroundColor: colors.TRANSPARENT,
        borderStyle: 'solid',
        borderLeftWidth: 9,
        borderRightWidth: 9,
        borderBottomWidth: 10,
        borderLeftColor: colors.TRANSPARENT,
        borderRightColor: colors.TRANSPARENT,
        borderBottomColor: colors.YELLOW.secondary,
        transform: [
            { rotate: '180deg' }
        ]
    },
    rideDesc: {
        flexDirection: 'column',
        backgroundColor:'red',
        margin:10,
        borderRadius:10,
        backgroundColor: colors.WHITE,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: {
            height: 0,
            width: 0
        },
    },
    userDesc: {
        flexDirection: 'row',
        margin:6,
        alignItems: 'center'
    },
    userView: {
        flexDirection: 'column',
        paddingLeft: 5,
    },

    fareView: {
        flexDirection: 'column',
        backgroundColor:'#f4f4f4',
        paddingHorizontal:10,
        paddingVertical:6,
        borderRadius:15,
    },
    locationView: {
        flex: 1,
        flexDirection: 'row',
        paddingHorizontal: 10,
        padding: 10,
        marginVertical: 14,
        justifyContent: "space-between",
    },
    locationView2: {
        flex: 1,
        flexDirection: 'row',
        // paddingHorizontal: 10,
        padding: 10,
        marginVertical: 14,

    },
    // callButtonStyle:{
    // width:400
    // },
    location: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginVertical: 0
    },
    greenDot: {
        backgroundColor: colors.GREEN.default,
        width: 10,
        height: 10,
        borderRadius: 50,
        alignSelf: 'flex-start',
        marginTop: 5
    },
    redDot: {
        backgroundColor: colors.RED,
        width: 10,
        height: 10,
        borderRadius: 50,
        alignSelf: 'flex-start',
        marginTop: 5
    },
    address: {
        flexDirection: 'row',
        flexGrow: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: 0,
        marginLeft: 6
    },
    billView: {
        marginVertical: 4
    },
    billTitle: {
        fontSize: 14,
        color: colors.GREY.default,
        fontFamily: 'Roboto-Bold',
        alignSelf:'center',
        marginVertical: 5
    },
    billOptions: {
        marginHorizontal: 10,
    },
    billItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        marginVertical: 5
    },
    billName: {
        fontSize: 12,
        fontFamily: 'Roboto-Bold',
        color: colors.GREY.default
    },
    billAmount: {
        fontSize: 12,
        fontFamily: 'Roboto-Medium',
        color: colors.GREY.default
    },
    discountAmount: {
        fontSize: 12,
        fontFamily: 'Roboto-Medium',
        color: colors.RED
    },


    grandAmount: {
        fontSize: 12,
        fontFamily: 'Roboto-Medium',
        color: '#af31f8'
    },

    billAmount2: {
        fontWeight: 'bold',
        fontSize: 12,
        fontFamily: 'Roboto-Bold',
        color: colors.GREY.default
    },
    backgroundImage: {
        flex: 1,
        width: '100%',
        height: 2,
    },
    carNoStyle: {
        fontSize: 12,
        //fontWeight: 'bold', 
        fontFamily: 'Roboto-Medium'
    },
    carNoStyleSubText: {
        fontSize: 12,
        //fontWeight: 'bold', 
        fontFamily: 'Roboto-Regular',
        color: colors.GREY.default
    },
    textStyle: {
        fontSize: 13,
        //fontWeight: 'bold', 
        fontFamily: 'Roboto-Medium'
    },
    mainView: {
        flex: 1,
        backgroundColor: colors.WHITE,
        //marginTop: StatusBar.currentHeight 
    },
    personStyle: {
        fontSize: 12,
        //fontWeight: 'bold', 
        color: colors.BLACK,
        fontFamily: 'Roboto-Medium'
    },
    personTextView: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    ratingText: {
        fontSize: 16,
        color: colors.GREY.iconSecondary,
        marginRight: 8,
        fontFamily: 'Roboto-Regular'
    },
    avatarView: {
        marginVertical: 0
    },
    timeStyle: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        marginTop: 1
    },
    adressStyle: {
        marginLeft: 6,
        fontSize: 10,
        lineHeight: 20
    },
    billView: {
        paddingHorizontal: 14
    },
    billText: {
        fontFamily: 'Roboto-Bold'
    },
    taxColor: {
        color: colors.GREY.default
    },
    paybleAmtView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    iosView: {
        paddingVertical: 10
    },
    dashView: {
        width: width, height: 1
    },
    paymentTextView: {
        paddingHorizontal: 10
    },
    // callButtonStyle:{
    //     width:50+'%'
    // },
    callButtonContainerStyle1: {
        flex: 1,
        width: '80%',
        height: 100
    },
    callButtonContainerStyle2: {
        flex: 1,
        width: '80%',
        height: 100,
        paddingLeft: 10
    },
    paynowButton: {
        flex: 1,
        width: '80%',
        height: 150,
        paddingLeft: 10
    },

    finalFare: {
        flex:1,
        justifyContent:'center', 
        alignItems:'center', 
        backgroundColor:'#f8f8f8', 
        marginVertical:6, 
        marginHorizontal:10, 
        padding:15, 
        borderRadius:30,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: {
            height: 0,
            width: 0
        },
    }
});