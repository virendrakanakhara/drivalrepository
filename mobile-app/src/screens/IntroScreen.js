import React, { useEffect, useRef, useContext, useState } from "react";
import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  Text,
  Dimensions,
  Linking,
  Modal,
  Platform,
  Alert,
  TouchableHighlight,
  AsyncStorage,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Video, AVPlaybackStatus } from "expo-av";
import * as Facebook from "expo-facebook";
import { language } from "config";
import * as AppleAuthentication from "expo-apple-authentication";
import * as Crypto from "expo-crypto";
import { TouchableOpacity } from "react-native-gesture-handler";
import { facebookAppId, features } from "config";
import { useSelector, useDispatch } from "react-redux";
import { FirebaseContext } from "common/src";
import { colors } from "../common/theme";

export default function IntroScreen(props) {
  const { api } = useContext(FirebaseContext);
  const { facebookSignIn, appleSignIn, clearLoginError } = api;
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const settings = useSelector((state) => state.settingsdata.settings);
  const pageActive = useRef(false);

  const [modalVisible, setModalVisible] = useState(false);
  const [delayFinish, setDelayFinish] = useState(false);
  useEffect(() => {
    const value = AsyncStorage.getItem("once");
    setTimeout(() => setDelayFinish(true), 1000);
    if (value !== null) {
      value
        .then((ret) => {
          if (ret === null) {
            // this is the first time
            // show the modal
            // save the value

            AsyncStorage.setItem("once", "yes");
            setModalVisible(true);
          }
        })
        .catch((err) => alert(err.toString()));
    }
  }, []);

  useEffect(() => {
    if (auth.info && pageActive.current) {
      pageActive.current = false;
      props.navigation.navigate("AuthLoading");
    }
    if (
      auth.error &&
      auth.error.msg &&
      pageActive.current &&
      auth.error.msg.message !== language.not_logged_in
    ) {
      pageActive.current = false;
      Alert.alert(language.alert, auth.error.msg.message);
      dispatch(clearLoginError());
    }
  }, [auth.info, auth.error]);

  const FbLogin = async () => {
    console.log("facebook id : " + facebookAppId);
    try {
      await Facebook.initializeAsync({ appId: facebookAppId });
      const { type, token } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ["public_profile", "email"],
      });
      if (type === "success") {
        pageActive.current = true;
        dispatch(facebookSignIn(token));
      } else {
        Alert.alert(language.alert, language.facebook_login_auth_error);
      }
    } catch ({ message }) {
      Alert.alert(language.alert, `${message}`);
    }
  };

  const AppleLogin = async () => {
    const csrf = Math.random().toString(36).substring(2, 15);
    const nonce = Math.random().toString(36).substring(2, 10);
    const hashedNonce = await Crypto.digestStringAsync(
      Crypto.CryptoDigestAlgorithm.SHA256,
      nonce
    );
    try {
      const applelogincredentials = await AppleAuthentication.signInAsync({
        requestedScopes: [
          AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
          AppleAuthentication.AppleAuthenticationScope.EMAIL,
        ],
        state: csrf,
        nonce: hashedNonce,
      });

      pageActive.current = true;
      dispatch(
        appleSignIn({
          idToken: applelogincredentials.identityToken,
          rawNonce: nonce,
        })
      );
    } catch (e) {
      if (e.code === "ERR_CANCELED") {
        Alert.alert(language.alert, language.apple_signin_error);
      }
    }
  };

  const onPressLoginEmail = async () => {
    pageActive.current = false;
    props.navigation.navigate("Login");
  };

  const onPressRegister = async () => {
    pageActive.current = false;
    props.navigation.navigate("Reg");
  };

  const openTerms = async () => {
    Linking.openURL(settings.CompanyTerms).catch((err) =>
      console.error("Couldn't load page", err)
    );
  };

  return (
    <ImageBackground resizeMode="stretch" style={""}>
      <View style={[styles.topSpace]}>
        <Video
          source={require("../../assets/video/intro.mp4")}
          style={{ flex: 1 }}
          rate={1}
          shouldPlay={true}
          isLooping={true}
          volume={1}
          muted={true}
          resizeMode="cover"
        />
      </View>

      {/* Login Button */}

      <TouchableOpacity onPress={onPressLoginEmail}>
        <LinearGradient
          // Button Linear Gradient
          colors={["#b329ff", "#38ccfc"]}
          start={[0, 1]}
          end={[1, 0]}
          style={styles.loginButton}
        >
          <Text
            //style={styles.text}
            style={styles.textButton}
          >
            {language.login}
          </Text>
        </LinearGradient>
      </TouchableOpacity>

      {features.MobileLoginEnabled ? (
        <TouchableOpacity onPress={onPressRegister}>
          <LinearGradient
            // Button Linear Gradient
            colors={["#38ccfc", "#b329ff"]}
            start={[0, 1]}
            end={[1, 0]}
            style={styles.regButton}
          >
            <Text
              //style={styles.text}
              onPress={onPressRegister}
              style={styles.textButton}
            >
              {language.register}
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      ) : null}

      {(Platform.OS == "ios" && features.AppleLoginEnabled) ||
      features.FacebookLoginEnabled ? (
        <View style={styles.seperator}>
          <View style={styles.lineLeft}></View>
          <View style={styles.lineLeftFiller}>
            <Text style={styles.sepText}>{language.spacer_message}</Text>
          </View>
          <View style={styles.lineRight}></View>
        </View>
      ) : null}

      {(Platform.OS == "ios" && features.AppleLoginEnabled) ||
      features.FacebookLoginEnabled ? (
        <View style={styles.socialBar}>
          {features.FacebookLoginEnabled ? (
            <TouchableOpacity style={styles.socialIcon} onPress={FbLogin}>
              <Image
                source={require("../../assets/images/image_fb.png")}
                resizeMode="contain"
                style={styles.socialIconImage}
              ></Image>
            </TouchableOpacity>
          ) : null}
          {Platform.OS == "ios" && features.AppleLoginEnabled ? (
            <TouchableOpacity style={styles.socialIcon} onPress={AppleLogin}>
              <Image
                source={require("../../assets/images/image_apple.png")}
                resizeMode="contain"
                style={styles.socialIconImage}
              ></Image>
            </TouchableOpacity>
          ) : null}
        </View>
      ) : null}

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible && delayFinish}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>
              {`DRIVAL LOCATION PERMISSIONS

This app is collecting location data permission  to enable you (Pickup location, Drop destination, Booking)

Please be informed that the 'location' for our app is used 

- For Drivers app will continue run in  'background' even when 'the app is closed' it will be always in use  to receive new rides

- For customers might be your location  is still running in the background to live track your full trip details

- To allow app collecting your location, please accept the permission location in Homescreen to pickup your curreny location

- For drivers once you switch your status to off.. background location will be autoclose, otherwise it will continue run in background`}
            </Text>

            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
            >
              <Text style={styles.textStyleModal}>Ok,I understand</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imagebg: {
    position: "absolute",
    left: 0,
    top: 0,
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
  topSpace: {
    marginTop: 0,
    marginLeft: 0,
    marginRight: 0,
    height: Dimensions.get("window").height * 0.58,
    width: Dimensions.get("window").width,
  },
  materialButtonDark: {
    height: 40,
    marginTop: 20,
    marginLeft: 35,
    marginRight: 35,
    backgroundColor: colors.GREY.iconSecondary,
  },
  materialButtonDark2: {
    height: 40,
    marginTop: 14,
    marginLeft: 35,
    marginRight: 35,
    backgroundColor: colors.GREY.iconSecondary,
  },
  loginButton: {
    height: 40,
    borderRadius: 20,
    marginTop: 14,
    alignItems: "center",
    marginLeft: 50,
    marginRight: 50,
  },

  regButton: {
    height: 40,
    borderRadius: 20,
    marginTop: 14,
    alignItems: "center",
    marginLeft: 50,
    marginRight: 50,
  },

  textButton: {
    alignSelf: "center",
    padding: "4%",
    color: "white",
    fontWeight: "700",
  },
  actionLine: {
    height: 20,
    flexDirection: "row",
    marginTop: 20,
    alignSelf: "center",
  },
  actionItem: {
    height: 20,
    marginLeft: 15,
    marginRight: 15,
    alignSelf: "center",
  },
  actionText: {
    fontSize: 15,
    fontFamily: "Roboto-Regular",
    fontWeight: "bold",
  },
  seperator: {
    width: 250,
    height: 20,
    flexDirection: "row",
    marginTop: 20,
    alignSelf: "center",
  },
  lineLeft: {
    width: 40,
    height: 1,
    backgroundColor: "rgba(113,113,113,1)",
    marginTop: 9,
  },
  sepText: {
    color: colors.BLACK,
    fontSize: 16,
    fontFamily: "Roboto-Regular",
  },
  lineLeftFiller: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
  },
  lineRight: {
    width: 40,
    height: 1,
    backgroundColor: "rgba(113,113,113,1)",
    marginTop: 9,
  },
  socialBar: {
    height: 40,
    flexDirection: "row",
    marginTop: 15,
    alignSelf: "center",
  },
  socialIcon: {
    width: 40,
    height: 40,
    marginLeft: 15,
    marginRight: 15,
    alignSelf: "center",
  },
  socialIconImage: {
    width: 40,
    height: 40,
  },
  terms: {
    marginTop: 18,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    opacity: 0.54,
  },

  /*modal style */
  /*modal popup styles*/
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyleModal: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
