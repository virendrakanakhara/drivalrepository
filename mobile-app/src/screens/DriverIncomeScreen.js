import React, {useState,useEffect} from 'react';
import { Header } from 'react-native-elements';
import { colors } from '../common/theme';
import { 
    StyleSheet,
    View,
    Text,
    TouchableWithoutFeedback
} from 'react-native';
import { language } from 'config';
import { useSelector } from 'react-redux';
import { RideList } from '../components';

export default function DriverIncomeScreen(props) {

    const auth = useSelector(state => state.auth);
    const bookings = useSelector(state => state.bookinglistdata.bookings);
    const [bookingData,setBookingData] = useState([]);
    const settings = useSelector(state => state.settingsdata.settings);
    const [totalEarning,setTotalEarning] = useState(0);
    const [today,setToday] = useState(0);
    const [thisMonth, setThisMonth] = useState(0);

    useEffect(()=>{
        if(bookings){
            let today =  new Date();
            let tdTrans = 0;
            let mnTrans = 0;
            let totTrans = 0;
            for(let i=0;i<bookings.length;i++){
                const {tripdate,driver_share} = bookings[i];
                let tDate = new Date(tripdate);
                if(driver_share != undefined){
                    if(tDate.getDate() === today.getDate() && tDate.getMonth() === today.getMonth()){
                        tdTrans  = tdTrans + driver_share;
                    }          
                    if(tDate.getMonth() === today.getMonth() && tDate.getFullYear() === today.getFullYear()){
                        mnTrans  = mnTrans + driver_share;
                    }
                    totTrans  = totTrans + driver_share; 
                }
            }
            setTotalEarning(totTrans);
            setToday(tdTrans);
            setThisMonth(mnTrans);   
            setBookingData(bookings);
        }else{
            setTotalEarning(0);
            setToday(0);
            setThisMonth(0);
            setBookingData([]);
        }
    },[bookings]);

    goDetails = (item, index) => {
        if (item && item.trip_cost > 0) {
            item.roundoffCost = Math.round(item.trip_cost).toFixed(2);
            item.roundoff = (Math.round(item.roundoffCost) - item.trip_cost).toFixed(2);
            props.navigation.push('RideDetails', { data: item });
        } else {
            item.roundoffCost = Math.round(item.estimate).toFixed(2);
            item.roundoff = (Math.round(item.roundoffCost) - item.estimate).toFixed(2);
            props.navigation.push('RideDetails', { data: item });
        }
    }

    return (
        <View style={styles.mainView}>
            <Header 
                backgroundColor={colors.GREY.default}
                leftComponent={{icon:'md-menu', type:'ionicon', color:colors.WHITE, size: 30, component: TouchableWithoutFeedback,onPress: ()=>{props.navigation.toggleDrawer();} }}
                centerComponent={<Text style={styles.headerTitleStyle}>{language.incomeText}</Text>}
                containerStyle={styles.headerStyle}
                innerContainerStyles={{marginLeft:10, marginRight: 10}}
            />
            <View style={styles.bodyContainer}>
               
                <View style={styles.listContainer}>
                    <View style={styles.totalEarning}>
                    <Text style={styles.todayEarningHeaderText2}>Total Trips</Text>
                    <Text style={styles.todayEarningMoneyText2}>{bookingData.length}</Text>
                    </View>
                    <View style={styles.thismonthEarning}>
                    <Text style={styles.todayEarningHeaderText2}>Collected Cash</Text>
                    <Text style={styles.todayEarningMoneyText2}>{settings.symbol}{totalEarning?parseFloat(totalEarning).toFixed(2):'0'}</Text>
                    </View>
                    <View style={styles.thismonthEarning}>
                    <Text style={styles.todayEarningHeaderText2}>{language.wallet_ballance}</Text>
                    <Text style={styles.todayEarningMoneyText2}>{settings.symbol}{auth.info && auth.info.profile ? parseFloat(auth.info.profile.walletBalance).toFixed(2) : '0'}</Text>
                    </View>
                </View>
                { <View style={styles.listContainer}>
                <View style={styles.totalFinalEarning}>
                    <Text style={styles.todayEarningHeaderText2}>{language.totalearning}</Text>
                    <Text style={styles.todayEarningMoneyText2}>{settings.symbol}{auth.info && auth.info.profile ? parseFloat(parseFloat(auth.info.profile.walletBalance).toFixed(2) - parseFloat(totalEarning).toFixed(2)).toFixed(2) : '0'}</Text>
                </View>
                </View>
                 }
            </View>
            <View style={styles.TripHeadingView}><Text style={styles.TripHeadingText}>Trips</Text></View>
            <RideList onPressButton={(item, index) => { goDetails(item, index) }} data={bookingData}></RideList>
        </View>
    );
}

const styles = StyleSheet.create({
    mainView:{ 
        flex:1, 
        backgroundColor: colors.WHITE, 
    } ,
    headerStyle: { 
        backgroundColor: colors.GREY.default, 
        borderBottomWidth: 0 
    },
    headerTitleStyle: { 
        color: colors.WHITE,
        fontFamily:'Roboto-Bold',
        fontSize: 20
    },
    bodyContainer:{
        
        
        flexDirection:'column'
    },
    todaysIncomeContainer:{
        
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: colors.YELLOW.light,
    },
    listContainer:{
        
        backgroundColor: colors.WHITE,
        marginTop:1,
        flexDirection:'row',
        paddingHorizontal:6,
        paddingVertical:6,
        paddingBottom:6,
        justifyContent:'space-between',
        alignItems:'flex-start'
    },
    todayEarningHeaderText:{
        fontSize:20,
        paddingBottom:5,
        color:colors.GREEN.bright
    },
    todayEarningMoneyText:{
        fontSize:55,
        fontWeight:'bold',
        color:colors.GREEN.bright 
    },
    totalEarning:{
       height:50,
       width:'32%',
       backgroundColor: '#af31f8',
       borderRadius:6,
       justifyContent:'center',
       alignItems:'center',
    },
    totalFinalEarning:{
        height:50,
        width:'100%',
        backgroundColor: '#af31f8',
        borderRadius:6,
        justifyContent:'center',
        alignItems:'center',
     },
    thismonthEarning:{
        height:50,
        width:'32%',
        backgroundColor: '#af31f8',
        borderRadius:6,
        justifyContent:'center',
        alignItems:'center',
    },
    todayEarningHeaderText2:{
        fontSize:13,
        paddingBottom:5,
        color: colors.GREY.Smoke_Grey
    },
    todayEarningMoneyText2:{
        fontSize:16,
        fontWeight:'bold',
        color: colors.WHITE
    },
    TripHeadingView:{
         height:50,
         width:"94%",
         marginHorizontal:10,
         backgroundColor:colors.GREY.Smoke_Grey,
         marginTop:5
    },
    TripHeadingText:{
        fontSize:16,
        fontWeight:"bold",
        paddingLeft:10,
        color :  '#af31f8',
        lineHeight:50,
        fontFamily: 'Roboto-Regular',
        

    }
})