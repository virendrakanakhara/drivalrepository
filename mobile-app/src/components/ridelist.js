import React from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity, Image,Linking } from 'react-native';
import { Icon } from 'react-native-elements'
import { colors } from '../common/theme';
import { language, dateStyle } from 'config';
import { useSelector } from 'react-redux';

export default function RideList(props) {

    const settings = useSelector(state => state.settingsdata.settings);

    const onPressButton = (item, index) => {
        props.onPressButton(item, index)
    }
    const onPressTicketButton = () => {
       
        props.onPressTicketButton()

       //Linking.openURL('https://www.drival-egypt.com/support/?a=add')
    }

    const renderData = ({ item, index }) => {
        return (
            <>
            <TouchableOpacity style={styles.iconClickStyle} onPress={() => onPressButton(item, index)}>
                
                <View style={styles.flexViewStyle}>
                    <View style={''}>


                        { /* FIRST ROW FLEX TWO */ }
                        <View style={{flexDirection:"row", justifyContent:'center'}}>
                            
                            {/* DATE DETAIL */}
                            <View style={{flex:5, flexDirection:'row'}}>

                                <Icon
                                            name='clock'
                                            type='feather'
                                            
                                            color={'#af31f8'}
                                            size={16}
                                            
                                />

                                <Text style={[styles.textStyle, styles.labelStyle,{paddingLeft:5}]}>
                                    TripDate:
                                </Text>

                                <Text style={[styles.textStyle, styles.dateStyle,{paddingLeft:5}]}>
                                    {item.bookingDate ? new Date(item.bookingDate).toLocaleString(dateStyle) : ''}
                                </Text>
                                
                            </View>



                            {/* CANCEL PHOTO DETAIL */}

                            
                            <View style={{flex:1, flexDirection:'row', justifyContent:'flex-end', position:'relative', right:10}}>
                                
                                
                                {
                                item.status == 'CANCELLED' ?
                                    <Image
                                        style={styles.cancelImageStyle}
                                        source={require('../../assets/images/cancel.jpg')}
                                    />
                                    :
                                    null
                                }
                                
                            </View>
                            
                            
                        </View>  



                        { /* SECOND ROW FLEX FULL */ }
                        {/* CAR DETAIL */}
                        <View style={{flexDirection:"row", justifyContent:'flex-start', flex:1, marginTop:8}}>

                            <Icon
                                            name='directions-car'
                                            color={'#af31f8'}
                                            size={16}
                                            
                                />
                            <Text style={[styles.textStyle,{paddingLeft:5}]}>
                            {item.carType ? item.carType : null} - {item.vehicle_number ? item.vehicle_number : language.no_car_assign_text}
                            </Text>
                            
                            
                        </View>  
                        

                        {/* THIRD ROW FULL */}
                        <View style={{flexDirection:"row", justifyContent:'center', flex:1, marginTop:15,}}>

                        {item.status != 'CANCELLED'  &&
                            <Text style={{fontFamily:'Roboto-Bold', fontSize:22}}>
                           {settings.symbol} {parseFloat(parseFloat(item.customer_paid).toFixed(2) - parseFloat(item.discount).toFixed(2)).toFixed(2)}
                            </Text>
                            
                        }
                        {item.status == 'CANCELLED'  &&
                            <Text style={{fontFamily:'Roboto-Bold', fontSize:22}}>
                           {settings.symbol} 0.00
                            </Text>
                            
                        }


                        </View> 

                        
                        <View style={[styles.picupStyle, styles.position]}>

                            <View >
                            <Icon
                                name='gps-fixed'
                                
                                color={'#58b950'}
                                size={16}
                                containerStyle={{ flex: 1 }}
                            />
                                </View>
                            <Text style={[styles.picPlaceStyle, styles.placeStyle]}>{item.pickup ? item.pickup.add : language.not_found_text}</Text>
                        </View>
                        <View style={[styles.dropStyle, styles.textViewStyle]}>
                        <View >
                            <Icon
                                name='map-pin'
                                type='feather'
                                
                                color={'red'}
                                size={16}
                                containerStyle={{ flex: 1 }}
                            />
                                </View>
                            <Text style={[styles.dropPlaceStyle, styles.placeStyle]}>{item.drop ? item.drop.add : language.not_found_text}</Text>
                            
                        </View>



                        {/* FORTH ROW FULL REPORT */}
                        <View style={{flexDirection:"row", justifyContent:'center', flex:1, marginTop:10,}}>
                        
                        <TouchableOpacity  onPress={()=>{onPressTicketButton()}}>
                            <View style={{backgroundColor:'#af31f8',width:"100%",width:"100%",padding:5,borderRadius:3,justifyContent:"center",alignItems:"center"}}>
                                <Text style={{color:"white"}}>Report
                                </Text>
                            </View>
                        </TouchableOpacity>
           
                        </View> 
                        
                    </View>
                    <View style={styles.textView2}>
                        <Text style={[styles.fareStyle1, styles.dateStyle]}>{item.status == 'NEW' ? language[item.status] : null}</Text>
                        <Text style={[styles.fareStyle2, styles.dateStyle]}>{item.status == 'PAID' || item.status == 'COMPLETE'? item.customer_paid ? settings.symbol + parseFloat(item.customer_paid).toFixed(2) : settings.symbol + parseFloat(item.estimate).toFixed(2) : null}</Text>
                        
                    </View>
                    
                </View>
            </TouchableOpacity>
            </>
        )
    }


    return (
        <View style={styles.textView3}>
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={props.data}
                renderItem={renderData}
            />
        </View>
    );

};

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 12,
    },
    fareStyle: {
        fontSize: 18,
       
    },
    fareStyle1: {
        fontSize: 18,
        textAlign:"right",
    },
    fareStyle2: {
        fontSize: 18,
        
        textAlign:"right",
        marginTop:5
    },
    carNoStyle: {
        marginLeft: 45,
        
        marginTop: 0,
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
    },
    picupStyle: {
        flexDirection: 'row',
       
    },
    picPlaceStyle: {
        color: colors.GREY.secondary,
        
        color:colors.BLACK
    },
    dropStyle: {
        flexDirection: 'row',
        
    },
    drpIconStyle: {
        color: colors.RED,
        fontSize: 20
    },
    dropPlaceStyle: {
        color: colors.GREY.secondary,
        
        color:colors.BLACK
    },
    greenDot: {
        alignSelf: 'center',
        borderRadius: 10,
        width: 10,
        height: 10,
        backgroundColor: colors.GREEN.default
    },
    redDot: {
        borderRadius: 10,
        width: 10,
        height: 10,
        backgroundColor: colors.RED

    },
    logoStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    iconClickStyle: {
        flex: 1,
        marginHorizontal:10,
        marginVertical:7,
        borderRadius:10,
        backgroundColor:colors.WHITE,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: {
            height: 0,
            width: 0
        },
        flexDirection: 'row'
    },
    flexViewStyle: {
        flex: 1,
        flexDirection: 'row',
        marginHorizontal:25,
        marginVertical:15,
    },
    dateStyle: {
        fontFamily: 'Roboto-Regular',
        color: '#af31f8',
        fontSize: 12,
    },

    labelStyle: {
        fontFamily: 'Roboto-Bold',
        color: colors.GREY.default,
        fontSize: 12,
    },

    carNoStyle: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        marginTop: 8,
        color: colors.GREY.default
    },
    placeStyle: {
        marginLeft: 5,
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        alignSelf: 'center'
    },
    textViewStyle: {
        marginTop: 10,
        marginBottom: 10
    },
    cancelImageStyle: {
        width: 40,
        height: 20,

    },
    iconViewStyle: {
        flex: 1, marginTop: 10
    },
    textView1: {
        flex: 5
    },
    textView2: {
        flex: 2,
        
        
    },
    textView3: {
        flex: 1
    },
    position: {
        marginTop: 20
    },
    textPosition: {
        alignSelf: 'center'
    }
});