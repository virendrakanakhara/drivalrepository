import {
  FETCH_PROMODATA,
  FETCH_PROMODATA_SUCCESS,
  FETCH_PROMODATA_FAILED,
  EDIT_PROMODATA
} from "../store/types";
import Polyline from '@mapbox/polyline';

import { FareCalculator } from '../other/FareCalculator';
import { getRouteDetails } from '../other/GoogleAPIFunctions';
import { store } from '../store/store';

export const fetchPromodata = () => (dispatch) => (firebase) => {
  
   const {
    userpromoRef
  } = firebase;
  
  
  dispatch({
    type: FETCH_PROMODATA,
    payload: null
  });
  userpromoRef.on("value", snapshot => {
   
    if (snapshot.val()) {
      const data = snapshot.val();
      const arr = Object.keys(data).map(i => {
        data[i].id = i;
        return data[i]
      });
      
      dispatch({
        type: FETCH_PROMODATA_SUCCESS,
        payload: arr
      });
    } else {
      dispatch({
        type: FETCH_PROMODATA_FAILED,
        payload: "No promos available."
      });
    }
  });
};
export const editPromodata = (promo, method) => (dispatch) => (firebase) => {
  
  const {
    userpromoRef, 
    userpromoEditRef
  } = firebase;
  dispatch({
    type: EDIT_PROMODATA,
    payload: { method, promo }
  });
  
  if (method === 'Add') {
    
        userpromoRef.push(promo).then((res) => {
         
      }).catch(error => {
         
             alert(error.code + ": " + error.message);
         
      });
  } else if (method === 'Delete') {
    userpromoEditRef(promo.id).remove();
  } else {
    userpromoEditRef(promo.id).set(promo);
  }
}

