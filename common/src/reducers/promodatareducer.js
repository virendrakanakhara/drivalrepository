import { 
  FETCH_PROMODATA,
  FETCH_PROMODATA_SUCCESS,
  FETCH_PROMODATA_FAILED,
  EDIT_PROMODATA
} from "../store/types";

export const INITIAL_STATE = {
  promos:null,
  loading: false,
  error:{
    flag:false,
    msg: null
  }
}

export const promodatareducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_PROMODATA:
      return {
        ...state,
        loading:true
      };
    case FETCH_PROMODATA_SUCCESS:
      
      return {
        ...state,
        promos:action.payload,
        loading:false
      };
    case FETCH_PROMODATA_FAILED:
      return {
        ...state,
        promos:null,
        loading:false,
        error:{
          flag:true,
          msg:action.payload
        }
      };
    case EDIT_PROMODATA:
      return state;
    default:
      return state;
  }
};