export const MainConfig = {
    AppDetails: {
        app_name: "DrivalEg", 
        app_description: "Drival EG Taxi App",
        app_identifier: "com.drival.egypt",
        ios_app_version: "1.0.0", 
        android_app_version: 101
    },
 
  FirebaseConfig : {
        apiKey: "AIzaSyCA9A3OYV8GdrlbUpoIB1V_fsxnGRUyN6Y",
        authDomain: "drivalegypt-app.firebaseapp.com",
        databaseURL: "https://drivalegypt-app-default-rtdb.firebaseio.com",
        projectId: "drivalegypt-app",
        storageBucket: "drivalegypt-app.appspot.com",
        messagingSenderId: "652586244369",
        appId: "1:652586244369:web:6e047b28fe4a48acf954b0"
      },
    Google_Map_Key: "AIzaSyC-aqJ_uFVL3k_C3IjUorkRcPEPAWuRtxs",
    facebookAppId: "154680066699959",
    PurchaseDetails: {
        CodeCanyon_Purchase_Code: "4e0b8d1f-7092-42c8-9d43-3c7894460cd1",
        Buyer_Email_Address: "Egycloud.co@gmail.com"
    }
}