import React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import styles from "assets/jss/material-kit-react/views/staticPages.js";
import Parallax from "components/Parallax/Parallax";
import Button from "components/CustomButtons/Button.js";
import { language } from 'config';


const dashboardRoutes = [];

const useStyles = makeStyles(styles);

export default function SuccessAddToWallet(props) {
  const classes = useStyles();
  const { ...rest } = props;


    

  
    

  return (
    <div>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
        {...rest}
      />
      <Parallax small filter image={require("assets/img/header-back.jpg")} />
      <div className={classNames(classes.main, classes.mainRaised)}>
 
        <div className={classes.container}>
            <br/>
            <h2 className={classes.title}>Success</h2>
            <p className={classes.description}>
            Please send us ticket with your Fawry receipt
            </p>
            
            <br/>
           

              <Button
            color="yellow"
            href="https://www.drival-egypt.com/support/?a=add"
            
            className={classes.navLink}
          >
            Go To Ticket System
          </Button>  
          <br />     
        </div>
        </div>

      <Footer />
    </div>
  );
}
